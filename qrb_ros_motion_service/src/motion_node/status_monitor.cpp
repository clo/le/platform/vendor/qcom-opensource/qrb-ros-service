// Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#include "qrb_ros_motion_service/motion_node/status_monitor.hpp"

#define LOW_POWER_LEVEL 22

using namespace std::placeholders;

namespace qrb_ros
{
namespace motion_service
{

StatusMonitorSubscriber::StatusMonitorSubscriber(std::shared_ptr<rclcpp::Node> node_handler,
    std::shared_ptr<MotionServiceProxy> service_proxy)
{
  RCLCPP_INFO(logger_, "create");
  node_handler_ = node_handler;
  motion_service_proxy_ = service_proxy;
  create_status_subscriber();
}

StatusMonitorSubscriber::~StatusMonitorSubscriber()
{
  RCLCPP_INFO(logger_, "destroy");
}

void StatusMonitorSubscriber::create_status_subscriber()
{
  RCLCPP_INFO(logger_, "create_status_subscriber");
  if (node_handler_ != nullptr) {
    auto battery_sub = node_handler_->create_subscription<sensor_msgs::msg::BatteryState>("battery",
        rclcpp::SystemDefaultsQoS(),
        std::bind(&StatusMonitorSubscriber::battery_subscriber_callback, this, _1));
    sub_ptr_.push_back(battery_sub);
    RCLCPP_INFO(logger_, "Create battery subscriber: topic=\"%s\"", battery_sub->get_topic_name());

    auto amr_status_sub = node_handler_->create_subscription<qrb_ros_amr_msgs::msg::AMRStatus>(
        "amr_status", rclcpp::SystemDefaultsQoS(),
        std::bind(&StatusMonitorSubscriber::amr_status_subscriber_callback, this, _1));
    sub_ptr_.push_back(amr_status_sub);
    RCLCPP_INFO(
        logger_, "Create amr status subscriber: topic=\"%s\"", amr_status_sub->get_topic_name());

    auto ex_sub = node_handler_->create_subscription<qrb_ros_robot_base_msgs::msg::Exception>(
        "robot_base_exception", rclcpp::SystemDefaultsQoS(),
        std::bind(&StatusMonitorSubscriber::exception_subscriber_callback, this, _1));
    sub_ptr_.push_back(ex_sub);
    RCLCPP_INFO(logger_, "Create exception subscriber: topic=\"%s\"", ex_sub->get_topic_name());
  }
}

void StatusMonitorSubscriber::battery_subscriber_callback(
    const sensor_msgs::msg::BatteryState::SharedPtr msg)
{
  RCLCPP_INFO(logger_, "battery_subscriber_callback");
  // handle low_power status
  auto voltage = msg->voltage;
  if (voltage <= LOW_POWER_LEVEL && !low_power_) {
    low_power_ = true;
    RCLCPP_INFO(logger_, "change low_power status ture");
    motion_service_proxy_->update_low_power_status(true);
  } else if (voltage > LOW_POWER_LEVEL && low_power_) {
    low_power_ = false;
    RCLCPP_INFO(logger_, "change low_power status false");
    motion_service_proxy_->update_low_power_status(false);
  }

  // handle force_charging status
  auto status = msg->power_supply_status;
  if (status == FORCE_CHARGING && !force_charging_) {
    force_charging_ = true;
    RCLCPP_INFO(logger_, "change force_charging status ture");
    motion_service_proxy_->update_charging_status(true);
  } else if (status == sensor_msgs::msg::BatteryState::POWER_SUPPLY_STATUS_CHARGING &&
             force_charging_) {
    force_charging_ = false;
    RCLCPP_INFO(logger_, "change force_charging status false");
    motion_service_proxy_->update_charging_status(false);
  }
}

void StatusMonitorSubscriber::amr_status_subscriber_callback(
    const qrb_ros_amr_msgs::msg::AMRStatus::SharedPtr msg)
{
  RCLCPP_INFO(logger_, "amr_status_subscriber_callback");
  int status_id = msg->status_change_id;
  // TODO
  if (status_id != 4) {
    return;
  }

  bool amr_status = false;
  int state = msg->current_state;
  if (state == ON_AE || state == ON_FOLLOW_PATH || state == FOLLOW_PATH_WAIT ||
      state == ON_P2PNAV || state == ON_ME || state == ME_DONE) {
    amr_status = true;
    RCLCPP_INFO(logger_, "amr_status is true");
  } else {
    amr_status = false;
    RCLCPP_INFO(logger_, "amr_status is false");
  }

  if (motion_service_proxy_ != nullptr) {
    motion_service_proxy_->update_navigation_status(amr_status);
  }
}

void StatusMonitorSubscriber::exception_subscriber_callback(
    const qrb_ros_robot_base_msgs::msg::Exception::SharedPtr msg)
{
  RCLCPP_INFO(logger_, "exception_subscriber_callback");
  if (msg->type != qrb_ros_robot_base_msgs::msg::Exception::EMERGENCY) {
    return;
  }

  bool exception_status = false;
  uint8_t event = msg->event;
  if (event == qrb_ros_robot_base_msgs::msg::Exception::ENTER) {
    exception_status = true;
  } else if (event == qrb_ros_robot_base_msgs::msg::Exception::EXIT) {
    exception_status = false;
  } else {
    RCLCPP_ERROR(logger_, "exception msg event error");
    return;
  }
  RCLCPP_INFO(logger_, "exception status is %s", exception_status ? "true" : "false");
  if (motion_service_proxy_ != nullptr) {
    motion_service_proxy_->update_diagnostic_status(exception_status);
  }
}

}  // namespace motion_service
}  // namespace qrb_ros
