// Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#include "qrb_ros_audio_service/voice_activation_client.hpp"

#include <csignal>

#define ROS_VOICE_ACTIVATION_CLIENT_NAME "ros_voice_activation_client"
#define VOICE_ACTIVATION_SERVICE_NAME "voice_activation_service"
#define VOICE_ACTIVATION_SERVICE_START "start_va"
#define VOICE_ACTIVATION_SERVICE_STOP "stop_va"
#define VOICE_ACTIVATION_SERVICE_LIST "list_va"
#define DUMP_VA_BUFFER_PARAMETER_NAME "dump_va_buffer"

#define ROSLOG(fmt, args...) RCLCPP_INFO(rclcpp::get_logger("RosVAClient"), fmt, ##args)
#define ROSLOGE(fmt, args...) RCLCPP_ERROR(rclcpp::get_logger("RosVAClient"), fmt, ##args)

#define WAIT_SERVICE_AVAIABLE_TIMEOUT (2)
#define WAIT_SERVICE_RESPONSE_TIMEOUT (5)

#define VA_EVENT_ERROR (0x100)
#define VA_EVENT_DETECTED (0x101)
#define VA_EVENT_DATA (0x102)

namespace qrb_ros
{
namespace audio_service
{

class WavWriter
{
  friend class RosVoiceActivationClient;

public:
  uint32_t write_data(uint8_t * data, uint32_t size)
  {
    if (!fp_) {
      return 0;
    }

    return fwrite(data, 1, size, fp_);
  }

  WavWriter(std::string file_path) : file_path_(file_path)
  {
    fp_ = fopen(file_path_.c_str(), "wb");

    if (!fp_) {
      ROSLOGE("open %s failed. reason: %s", file_path_.c_str(), strerror(errno));
    }
  }

  ~WavWriter()
  {
    if (fp_) {
      fclose(fp_);
    }
  }

private:
  FILE * fp_;
  std::string file_path_;
};

static RosVoiceActivationClient * s_client;
static std::mutex s_mtx_;

static void signal_handler(int sign)
{
  s_client->release_client();
  rclcpp::shutdown();
}

RosVoiceActivationClient::RosVoiceActivationClient(const rclcpp::NodeOptions & options)
  : Node(ROS_VOICE_ACTIVATION_CLIENT_NAME, options)
{
  qrb::audio_manager::Stream::register_callback(
      DOMAIN_ID_VOICE_ACTIVATION, RosVoiceActivationClient::command_arrived, false);
  declare_parameter(DUMP_VA_BUFFER_PARAMETER_NAME, false);
  ros_client_ = create_client<VoiceActivationService>(VOICE_ACTIVATION_SERVICE_NAME);
  s_client = this;

  signal(SIGINT, signal_handler);
  ROSLOG("%s started.", ROS_VOICE_ACTIVATION_CLIENT_NAME);
}

RosVoiceActivationClient::~RosVoiceActivationClient()
{
  all_client_info_.clear();
  ros_client_.reset();
  ROSLOG("%s stopped.", ROS_VOICE_ACTIVATION_CLIENT_NAME);
}

void RosVoiceActivationClient::release_client()
{
  while (all_client_info_.size() > 0) {
    stop_va(all_client_info_.begin()->first);
  }
}

bool RosVoiceActivationClient::is_service_available()
{
  return ros_client_->wait_for_service(std::chrono::seconds(WAIT_SERVICE_AVAIABLE_TIMEOUT));
}

std::shared_ptr<rclcpp::Subscription<VoiceActivationPublishData>>
RosVoiceActivationClient::subscribe_topic(std::string topic_name,
    std::function<void(VoiceActivationPublishData::UniquePtr)> topic_callback)
{
  return create_subscription<VoiceActivationPublishData>(topic_name.c_str(), 10, topic_callback);
}

int32_t RosVoiceActivationClient::send_reqeust(VoiceActivationService::Request::SharedPtr req,
    std::function<int32_t(VoiceActivationService::Response::SharedPtr)> response_callback)
{
  if (!is_service_available()) {
    ROSLOGE("%s(%d) exit: service is unavailable.", __func__, __LINE__);
    return -1;
  }

  auto result_future = ros_client_->async_send_request(req);
  ROSLOG("waiting response....");

  if (result_future.wait_for(std::chrono::seconds(WAIT_SERVICE_RESPONSE_TIMEOUT)) !=
      std::future_status::ready) {
    ROSLOGE("waiting response failed.");
    return -1;
  }

  ROSLOG("Got response.");
  if (response_callback) {
    return response_callback(result_future.get());
  }
  return 0;
}

int32_t RosVoiceActivationClient::start_va(VaInfoStruct * info, uint32_t * client_handle)
{
  ROSLOG("%s(%d) enter.", __func__, __LINE__);

  if (!info) {
    ROSLOGE("%s(%d) exit: info i null.", __func__, __LINE__);
    return -1;
  }

  *client_handle = 0;

  VoiceActivationService::Request::SharedPtr req(new VoiceActivationService::Request());
  std::shared_ptr<VoiceActivationClient> client_info(new VoiceActivationClient());

  req->sound_model_file = info->sound_model_file;
  req->num_phrases = info->num_phrases;
  req->phrase_hex_data = info->phrase_hex_data;
  req->second_stage_conf = info->second_stage_conf;
  req->user_hex_data = info->user_hex_data;
  req->sample_rate = info->sample_rate;
  req->channels = info->channels;
  req->opaque_enabled = info->opaque_enabled;
  req->lab_enabled = info->lab_enabled;
  req->lab_duration = info->lab_duration;
  req->pre_roll_duration = info->pre_roll_duration;
  req->history_buffer_duration = info->history_buffer_duration;
  req->vendor_uuid = info->vendor_uuid;
  req->publish_topic_name = info->publish_topic_name;
  req->user_defined_model = info->user_defined_model;
  req->oprt_cmd = VOICE_ACTIVATION_SERVICE_START;
  req->cookie = (uint32_t)((uint64_t)client_info.get());
  client_info->topic_name = info->publish_topic_name;

  uint32_t va_handle_from_server = 0;
  int32_t ret = send_reqeust(
      req, [&va_handle_from_server](VoiceActivationService::Response::SharedPtr resp) -> int32_t {
        va_handle_from_server = resp->va_handle;
        return resp->ret;
      });

  ROSLOG("ret: %d, va_handle: 0x%08X(%d)", ret, va_handle_from_server, va_handle_from_server);
  if (ret == 0) {
    client_info->va_handle = va_handle_from_server;
    *client_handle = req->cookie;
    if (client_info->topic_name != "") {
      client_info->subscription = subscribe_topic(
          client_info->topic_name, [this](VoiceActivationPublishData::UniquePtr msg) {
            if (all_client_info_.count(msg->cookie) == 0) {
              return;
            }

            if (msg->event_id == VA_EVENT_DETECTED) {
              ROSLOG("VA_EVENT_DETECTED arrived.");
            } else if (msg->event_id == VA_EVENT_DATA) {
              ROSLOG("VA_EVENT_DATA arrived. cookie: 0x%08X, buffer size: %d", msg->cookie,
                  msg->capture_buffer_size);
              std::shared_ptr<VoiceActivationClient> client_info = all_client_info_.at(msg->cookie);
              bool dump = get_parameter(DUMP_VA_BUFFER_PARAMETER_NAME).as_bool();
              on_subscribed(&(msg->capture_buffer[0]), msg->capture_buffer_size, msg->sample_rate,
                  msg->channels, client_info->topic_name, dump);
            } else {
              ROSLOGE("error happens on server node, event: 0x%X.", msg->event_id);
            }
          });
    }
    all_client_info_.insert({ req->cookie, client_info });
  }

  ROSLOG("%s(%d) exit: cookie: 0x%08X, ret(%d).", __func__, __LINE__, req->cookie, ret);
  return ret;
}

int32_t RosVoiceActivationClient::stop_va(uint32_t client_handle)
{
  ROSLOG("%s(%d) enter.", __func__, __LINE__);

  if (all_client_info_.count(client_handle) == 0) {
    ROSLOGE("%s(%d) exit: invalid client handle.", __func__, __LINE__);
    return -1;
  }

  std::shared_ptr<VoiceActivationService::Request> req(new VoiceActivationService::Request());
  std::shared_ptr<VoiceActivationClient> client_info = all_client_info_.at(client_handle);

  req->oprt_cmd = VOICE_ACTIVATION_SERVICE_STOP;
  req->va_handle = client_info->va_handle;

  int32_t ret =
      send_reqeust(req, [](std::shared_ptr<VoiceActivationService::Response> response) -> int32_t {
        return response->ret;
      });
  if (client_info->subscription.get()) {
    client_info->subscription.reset();
  }
  all_client_info_.erase(client_handle);

  ROSLOG("%s(%d) exit.", __func__, __LINE__);
  return ret;
}

void RosVoiceActivationClient::on_subscribed(uint8_t * data,
    uint32_t size,
    uint32_t sample_rate,
    uint32_t channels,
    std::string file_name,
    bool dump)
{
  if (!data || size == 0) {
    return;
  }

  ROSLOG("size: %d, data[%d]: %d, sr: %d, chn: %d", size, size - 1, data[size - 1], sample_rate,
      channels);
  if (dump) {
    std::shared_ptr<WavWriter> wav_writer(
        new WavWriter(std::string("/home/qrobot/" + file_name + "_out.wav")));
    wav_writer->write_data(data, size);

    ROSLOG("file: %s generated.", wav_writer->file_path_.c_str());
  }
}

bool RosVoiceActivationClient::command_arrived(const void * const payload,
    StreamCommand cmd,
    uint32_t & client_handle)
{
  std::unique_lock<std::mutex> lck(s_mtx_);
  if (!s_client) {
    return false;
  }

  bool res = false;
  if (cmd == StreamCommand::START) {
    ROSLOG("%s(%d) enter - start.", __func__, __LINE__);
    uint32_t handle = 0;
    int32_t ret = s_client->start_va((VaInfoStruct *)payload, &handle);
    if (ret == 0) {
      client_handle = handle;
    }

    res = ret == 0;
  } else if (cmd == StreamCommand::STOP) {
    ROSLOG("%s(%d) enter - stop.", __func__, __LINE__);
    res = s_client->stop_va(client_handle) == 0;
  }

  return res;
}

}  // namespace audio_service
}  // namespace qrb_ros

#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(qrb_ros::audio_service::RosVoiceActivationClient)
