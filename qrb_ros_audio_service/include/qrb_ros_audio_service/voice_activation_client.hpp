// Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#ifndef QRB_ROS_AUDIO_SERVICE__ROS_VOICE_ACTIVATION_HPP_
#define QRB_ROS_AUDIO_SERVICE__ROS_VOICE_ACTIVATION_HPP_

#include <map>
#include <memory>
#include <mutex>

#include "qrb_audio_manager/voice_activation_stream.hpp"
#include "qrb_ros_voice_activation_msgs/msg/voice_activation_publish_data.hpp"
#include "qrb_ros_voice_activation_msgs/srv/voice_activation.hpp"
#include "rclcpp/rclcpp.hpp"

using namespace qrb::audio_manager;

namespace qrb_ros
{
namespace audio_service
{

using VoiceActivationService = qrb_ros_voice_activation_msgs::srv::VoiceActivation;
using VoiceActivationPublishData = qrb_ros_voice_activation_msgs::msg::VoiceActivationPublishData;

typedef struct _VoiceActivationClient
{
  uint32_t va_handle;
  std::string topic_name;
  std::shared_ptr<rclcpp::Subscription<VoiceActivationPublishData>> subscription;
} VoiceActivationClient;

class RosVoiceActivationClient : public rclcpp::Node
{
public:
  explicit RosVoiceActivationClient(const rclcpp::NodeOptions & options);
  ~RosVoiceActivationClient();
  void release_client();

private:
  int32_t start_va(VaInfoStruct * info, uint32_t * client_handle);
  int32_t stop_va(uint32_t client_handle);
  bool is_service_available();
  std::shared_ptr<rclcpp::Subscription<VoiceActivationPublishData>> subscribe_topic(
      std::string topic_name,
      std::function<void(VoiceActivationPublishData::UniquePtr)> topic_callback);
  int32_t send_reqeust(VoiceActivationService::Request::SharedPtr req,
      std::function<int32_t(VoiceActivationService::Response::SharedPtr)> response_callback);

  static void on_subscribed(uint8_t * data,
      uint32_t size,
      uint32_t sample_rate,
      uint32_t channels,
      std::string file_name,
      bool dump);
  static bool command_arrived(const void * const payload,
      StreamCommand cmd,
      uint32_t & client_handle);

  std::shared_ptr<rclcpp::Client<VoiceActivationService>> ros_client_;
  std::map<uint32_t, std::shared_ptr<VoiceActivationClient>> all_client_info_;
};

}  // namespace audio_service
}  // namespace qrb_ros

#endif  // QRB_ROS_AUDIO_SERVICE__ROS_VOICE_ACTIVATION_HPP_
