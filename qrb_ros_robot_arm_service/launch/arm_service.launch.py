# Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear

from launch import LaunchDescription
from launch_ros.actions import ComposableNodeContainer
from launch_ros.descriptions import ComposableNode


def generate_launch_description():
    return LaunchDescription([ComposableNodeContainer(
        name='component_arm_service_container',
        namespace='',
        package='rclcpp_components',
        executable='component_container_mt',
        composable_node_descriptions=[
            # topic 
            ComposableNode(
                package='qrb_ros_robot_arm_service',
                plugin='qrb_ros::arm_service::LaserSubscriber',
                extra_arguments=[{'use_intra_process_comms': True}],
            ),
            ComposableNode(
                package='qrb_ros_robot_arm_service',
                plugin='qrb_ros::arm_service::SyetemStatusSubscriber',
                extra_arguments=[{'use_intra_process_comms': True}],
            ),
            # service
            ComposableNode(
                package='qrb_ros_robot_arm_service',
                plugin='qrb_ros::arm_service::ManipulatorControlClient',
                extra_arguments=[{'use_intra_process_comms': True}],
            ),
            ComposableNode(
                package='qrb_ros_robot_arm_service',
                plugin='qrb_ros::arm_service::ArmManualControlServer',
                extra_arguments=[{'use_intra_process_comms': True}],
            ),
            ComposableNode(
                package='qrb_ros_robot_arm_service',
                plugin='qrb_ros::arm_service::ArmRecordPlaybackFileServer',
                extra_arguments=[{'use_intra_process_comms': True}],
            ),
            ComposableNode(
                package='qrb_ros_robot_arm_service',
                plugin='qrb_ros::arm_service::PoseCompensationFileServer',
                extra_arguments=[{'use_intra_process_comms': True}],
            ),
            # action
            ComposableNode(
                package='qrb_ros_robot_arm_service',
                plugin='qrb_ros::arm_service::ArmRecordPlaybackActionServer',
                extra_arguments=[{'use_intra_process_comms': True}],
            ),
            ComposableNode(
                package='qrb_ros_robot_arm_service',
                plugin='qrb_ros::arm_service::MotionServiceCallerActionClient',
                extra_arguments=[{'use_intra_process_comms': True}],
            ),
            ComposableNode(
                package='qrb_ros_robot_arm_service',
                plugin='qrb_ros::arm_service::PoseCompensationActionServer',
                extra_arguments=[{'use_intra_process_comms': True}],
            ),
        ]
    )])
