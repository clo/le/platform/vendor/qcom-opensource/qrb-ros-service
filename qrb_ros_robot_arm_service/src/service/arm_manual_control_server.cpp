// Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#include "service/arm_manual_control_server.hpp"

using namespace std::placeholders;

namespace qrb_ros
{
namespace arm_service
{
ArmManualControlServer::ArmManualControlServer(const rclcpp::NodeOptions & options)
  : Node("arm_manual_control_server", options)
{
  this->callback_group_ = this->create_callback_group(rclcpp::CallbackGroupType::MutuallyExclusive);
  rclcpp::SubscriptionOptions sub_options;
  sub_options.callback_group = this->callback_group_;

  this->get_control_mode_server_ =
      this->create_service<SrvManipulatorGetControlMode>(this->get_control_mode_srv_name_,
          std::bind(&ArmManualControlServer::get_control_mode_cb_, this, _1, _2, _3),
          rmw_qos_profile_services_default, this->callback_group_);
  this->set_control_mode_server_ =
      this->create_service<SrvManipulatorSetControlMode>(this->set_control_mode_srv_name_,
          std::bind(&ArmManualControlServer::set_control_mode_cb_, this, _1, _2, _3),
          rmw_qos_profile_services_default, this->callback_group_);
  this->move_joint_pose_server_ =
      this->create_service<SrvManipulatorMoveJointPose>(this->move_joint_pose_srv_name_,
          std::bind(&ArmManualControlServer::move_joint_pose_cb_, this, _1, _2, _3),
          rmw_qos_profile_services_default, this->callback_group_);
  this->get_joint_pose_server_ =
      this->create_service<SrvManipulatorGetJointPose>(this->get_joint_pose_srv_name_,
          std::bind(&ArmManualControlServer::get_joint_pose_cb_, this, _1, _2, _3),
          rmw_qos_profile_services_default, this->callback_group_);
  this->move_tcp_pose_server_ =
      this->create_service<SrvManipulatorMoveTcpPose>(this->move_tcp_pose_srv_name_,
          std::bind(&ArmManualControlServer::move_tcp_pose_cb_, this, _1, _2, _3),
          rmw_qos_profile_services_default, this->callback_group_);
  this->get_tcp_pose_server_ =
      this->create_service<SrvManipulatorGetTcpPose>(this->get_tcp_pose_srv_name_,
          std::bind(&ArmManualControlServer::get_tcp_pose_cb_, this, _1, _2, _3),
          rmw_qos_profile_services_default, this->callback_group_);
  this->target_reachable_server_ =
      this->create_service<SrvManipulatorTargetReachable>(this->target_reachable_srv_name_,
          std::bind(&ArmManualControlServer::target_reachable_cb_, this, _1, _2, _3),
          rmw_qos_profile_services_default, this->callback_group_);
  this->claw_control_server_ =
      this->create_service<SrvManipulatorClawControl>(this->claw_control_srv_name_,
          std::bind(&ArmManualControlServer::claw_control_cb_, this, _1, _2, _3),
          rmw_qos_profile_services_default, this->callback_group_);
  this->get_claw_status_server_ =
      this->create_service<SrvManipulatorClawGetStatus>(this->claw_get_status_srv_name_,
          std::bind(&ArmManualControlServer::get_claw_status_cb_, this, _1, _2, _3),
          rmw_qos_profile_services_default, this->callback_group_);

  qrb::arm_manager::ArmManager & arm_manager_instance =
      qrb::arm_manager::ArmManager::get_instance();
  this->arm_manual_control_instance_ = arm_manager_instance.get_arm_manual_control_instance();
}

void ArmManualControlServer::get_control_mode_cb_(const std::shared_ptr<rmw_request_id_t>,
    const std::shared_ptr<SrvManipulatorGetControlMode::Request> request,
    std::shared_ptr<SrvManipulatorGetControlMode::Response> response)
{
  auto start_time = std::chrono::steady_clock::now();

  qrb::arm_manager::ManipulatorGetControlMode data;
  data.manipulator_id = request->manipulator_id;
  if (this->arm_manual_control_instance_->get_control_mode(data)) {
    response->result = data.mode;
  } else {
    RCLCPP_ERROR(this->get_logger(), "get control mode failed.");
    response->result = SrvManipulatorGetControlMode::Response::ERROR;
  }

  auto end_time = std::chrono::steady_clock::now();
  auto elapsed_time =
      std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time).count();
  RCLCPP_INFO(this->get_logger(), "get_control_mode latency: %ld us(microseconds)", elapsed_time);
}

void ArmManualControlServer::set_control_mode_cb_(const std::shared_ptr<rmw_request_id_t>,
    const std::shared_ptr<SrvManipulatorSetControlMode::Request> request,
    std::shared_ptr<SrvManipulatorSetControlMode::Response> response)
{
  auto start_time = std::chrono::steady_clock::now();

  qrb::arm_manager::ManipulatorSetControlMode data;
  data.manipulator_id = request->manipulator_id;
  data.mode = request->mode;
  if (this->arm_manual_control_instance_->set_control_mode(data)) {
    response->result = data.result;
  } else {
    RCLCPP_ERROR(this->get_logger(), "set control mode failed.");
    response->result = false;
  }

  auto end_time = std::chrono::steady_clock::now();
  auto elapsed_time =
      std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time).count();
  RCLCPP_INFO(this->get_logger(), "set_control_mode latency: %ld us(microseconds)", elapsed_time);
}

void ArmManualControlServer::move_joint_pose_cb_(const std::shared_ptr<rmw_request_id_t>,
    const std::shared_ptr<SrvManipulatorMoveJointPose::Request> request,
    std::shared_ptr<SrvManipulatorMoveJointPose::Response> response)
{
  auto start_time = std::chrono::steady_clock::now();

  qrb::arm_manager::ManipulatorMoveJointPose data;
  data.manipulator_id = request->manipulator_id;
  data.control_params.speed = request->speed;
  data.control_params.accleration = request->acc;
  data.control_params.time = request->time;
  data.control_params.radius = request->radius;
  data.joints_pose = std::move(request->joints_pose);
  data.joints_num = request->joints_num;
  if (this->arm_manual_control_instance_->move_joint_pose(data)) {
    response->result = data.result;
  } else {
    RCLCPP_ERROR(this->get_logger(), "move joint pose failed.");
    response->result = false;
  }

  auto end_time = std::chrono::steady_clock::now();
  auto elapsed_time =
      std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time).count();
  RCLCPP_INFO(this->get_logger(), "move_joint_pose latency: %ld us(microseconds)", elapsed_time);
}

void ArmManualControlServer::get_joint_pose_cb_(const std::shared_ptr<rmw_request_id_t>,
    const std::shared_ptr<SrvManipulatorGetJointPose::Request> request,
    std::shared_ptr<SrvManipulatorGetJointPose::Response> response)
{
  auto start_time = std::chrono::steady_clock::now();

  qrb::arm_manager::ManipulatorGetJointPose data;
  data.manipulator_id = request->manipulator_id;
  if (this->arm_manual_control_instance_->get_joint_pose(data)) {
    response->joints_pose = std::move(data.joints_pose);
    response->joints_num = data.joints_num;
    response->result = data.result;
  } else {
    RCLCPP_ERROR(this->get_logger(), "get joint pose failed.");
    response->result = false;
  }

  auto end_time = std::chrono::steady_clock::now();
  auto elapsed_time =
      std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time).count();
  RCLCPP_INFO(this->get_logger(), "get_joint_pose latency: %ld us(microseconds)", elapsed_time);
}

void ArmManualControlServer::move_tcp_pose_cb_(const std::shared_ptr<rmw_request_id_t>,
    const std::shared_ptr<SrvManipulatorMoveTcpPose::Request> request,
    std::shared_ptr<SrvManipulatorMoveTcpPose::Response> response)
{
  auto start_time = std::chrono::steady_clock::now();

  qrb::arm_manager::ManipulatorMoveTcpPose data;
  data.manipulator_id = request->manipulator_id;
  data.control_params.speed = request->speed;
  data.control_params.accleration = request->acc;
  data.control_params.time = request->time;
  data.control_params.radius = request->radius;
  data.tcp_pose.position_x = request->pose.position.x;
  data.tcp_pose.position_y = request->pose.position.y;
  data.tcp_pose.position_z = request->pose.position.z;
  data.tcp_pose.orientation_x = request->pose.orientation.x;
  data.tcp_pose.orientation_y = request->pose.orientation.y;
  data.tcp_pose.orientation_z = request->pose.orientation.z;
  data.tcp_pose.orientation_w = request->pose.orientation.w;
  if (this->arm_manual_control_instance_->move_tcp_pose(data)) {
    response->result = data.result;
  } else {
    RCLCPP_ERROR(this->get_logger(), "move tcp pose failed.");
    response->result = false;
  }

  auto end_time = std::chrono::steady_clock::now();
  auto elapsed_time =
      std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time).count();
  RCLCPP_INFO(this->get_logger(), "move_tcp_pose latency: %ld us(microseconds)", elapsed_time);
}

void ArmManualControlServer::get_tcp_pose_cb_(const std::shared_ptr<rmw_request_id_t>,
    const std::shared_ptr<SrvManipulatorGetTcpPose::Request> request,
    std::shared_ptr<SrvManipulatorGetTcpPose::Response> response)
{
  auto start_time = std::chrono::steady_clock::now();

  qrb::arm_manager::ManipulatorGetTcpPose data;
  data.manipulator_id = request->manipulator_id;
  if (this->arm_manual_control_instance_->get_tcp_pose(data)) {
    response->pose.position.x = data.tcp_pose.position_x;
    response->pose.position.y = data.tcp_pose.position_y;
    response->pose.position.z = data.tcp_pose.position_z;
    response->pose.orientation.x = data.tcp_pose.orientation_x;
    response->pose.orientation.y = data.tcp_pose.orientation_y;
    response->pose.orientation.z = data.tcp_pose.orientation_z;
    response->pose.orientation.w = data.tcp_pose.orientation_w;
    response->result = data.result;
  } else {
    RCLCPP_ERROR(this->get_logger(), "get tcp pose failed.");
    response->result = false;
  }

  auto end_time = std::chrono::steady_clock::now();
  auto elapsed_time =
      std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time).count();
  RCLCPP_INFO(this->get_logger(), "get_tcp_pose latency: %ld us(microseconds)", elapsed_time);
}

void ArmManualControlServer::target_reachable_cb_(const std::shared_ptr<rmw_request_id_t>,
    const std::shared_ptr<SrvManipulatorTargetReachable::Request> request,
    std::shared_ptr<SrvManipulatorTargetReachable::Response> response)
{
  auto start_time = std::chrono::steady_clock::now();

  qrb::arm_manager::ManipulatorTargetReachable data;
  data.manipulator_id = request->manipulator_id;
  data.tcp_pose.position_x = request->pose.position.x;
  data.tcp_pose.position_y = request->pose.position.y;
  data.tcp_pose.position_z = request->pose.position.z;
  data.tcp_pose.orientation_x = request->pose.orientation.x;
  data.tcp_pose.orientation_y = request->pose.orientation.y;
  data.tcp_pose.orientation_z = request->pose.orientation.z;
  data.tcp_pose.orientation_w = request->pose.orientation.w;
  if (this->arm_manual_control_instance_->check_target_reachable(data)) {
    response->result = data.result;
  } else {
    RCLCPP_ERROR(this->get_logger(), "check target reachable failed.");
    response->result = false;
  }

  auto end_time = std::chrono::steady_clock::now();
  auto elapsed_time =
      std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time).count();
  RCLCPP_INFO(
      this->get_logger(), "check_target_reachable latency: %ld us(microseconds)", elapsed_time);
}

void ArmManualControlServer::claw_control_cb_(const std::shared_ptr<rmw_request_id_t>,
    const std::shared_ptr<SrvManipulatorClawControl::Request> request,
    std::shared_ptr<SrvManipulatorClawControl::Response> response)
{
  auto start_time = std::chrono::steady_clock::now();

  qrb::arm_manager::ManipulatorClawControl data;
  data.manipulator_id = request->manipulator_id;
  data.amplitude = std::move(request->amplitude);
  data.force = std::move(request->force);
  if (this->arm_manual_control_instance_->control_claw(data)) {
    response->result = data.result;
  } else {
    RCLCPP_ERROR(this->get_logger(), "control claw failed.");
    response->result = false;
  }

  auto end_time = std::chrono::steady_clock::now();
  auto elapsed_time =
      std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time).count();
  RCLCPP_INFO(this->get_logger(), "control_claw latency: %ld us(microseconds)", elapsed_time);
}

void ArmManualControlServer::get_claw_status_cb_(const std::shared_ptr<rmw_request_id_t>,
    const std::shared_ptr<SrvManipulatorClawGetStatus::Request> request,
    std::shared_ptr<SrvManipulatorClawGetStatus::Response> response)
{
  auto start_time = std::chrono::steady_clock::now();

  qrb::arm_manager::ManipulatorClawGetStatus data;
  data.manipulator_id = request->manipulator_id;
  if (this->arm_manual_control_instance_->get_claw_status(data)) {
    response->amplitude = std::move(data.amplitude);
    response->force = std::move(data.force);
    response->result = data.result;
  } else {
    RCLCPP_ERROR(this->get_logger(), "get claw status failed.");
    response->result = false;
  }

  auto end_time = std::chrono::steady_clock::now();
  auto elapsed_time =
      std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time).count();
  RCLCPP_INFO(this->get_logger(), "get_claw_status latency: %ld us(microseconds)", elapsed_time);
}
}  // namespace arm_service
}  // namespace qrb_ros

#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(qrb_ros::arm_service::ArmManualControlServer)