// Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#include "service/arm_record_playback_file_server.hpp"

using namespace std::placeholders;

namespace qrb_ros
{
namespace arm_service
{
ArmRecordPlaybackFileServer::ArmRecordPlaybackFileServer(const rclcpp::NodeOptions& options)
  : Node("arm_service_record_playback_file_server", options)
{
  this->callback_group_ = this->create_callback_group(rclcpp::CallbackGroupType::MutuallyExclusive);
  rclcpp::SubscriptionOptions sub_options;
  sub_options.callback_group = this->callback_group_;

  this->check_file_server_ = this->create_service<ArmServiceCheckFile>(
      this->check_file_srv_name_,
      std::bind(&ArmRecordPlaybackFileServer::check_file_cb_, this, _1, _2, _3),
      rmw_qos_profile_services_default, callback_group_);
  this->delete_file_server_ = this->create_service<ArmServiceSaveDeleteFile>(
      this->delete_file_srv_name_,
      std::bind(&ArmRecordPlaybackFileServer::delete_file_cb_, this, _1, _2, _3),
      rmw_qos_profile_services_default, callback_group_);

  qrb::arm_manager::ArmManager& arm_manager_instance = qrb::arm_manager::ArmManager::get_instance();
  this->arm_record_playback_instance_ = arm_manager_instance.get_arm_record_playback_instance();
}

void ArmRecordPlaybackFileServer::check_file_cb_(
    const std::shared_ptr<rmw_request_id_t>,
    const std::shared_ptr<ArmServiceCheckFile::Request> request,
    std::shared_ptr<ArmServiceCheckFile::Response> response)
{
  response->result =
      this->arm_record_playback_instance_->check_record_file(request->file_id, response->valid_ids);
}

void ArmRecordPlaybackFileServer::delete_file_cb_(
    const std::shared_ptr<rmw_request_id_t>,
    const std::shared_ptr<ArmServiceSaveDeleteFile::Request> request,
    std::shared_ptr<ArmServiceSaveDeleteFile::Response> response)
{
  if (request->command == ArmServiceSaveDeleteFile::Request::CMD_DELETE) {
    response->result = this->arm_record_playback_instance_->delete_record_file(request->file_id);
  } else {
    response->result = false;
  }
}

}  // namespace arm_service
}  // namespace qrb_ros

#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(qrb_ros::arm_service::ArmRecordPlaybackFileServer)