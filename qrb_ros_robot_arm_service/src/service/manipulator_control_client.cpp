// Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#include "service/manipulator_control_client.hpp"

using namespace std::placeholders;

namespace qrb_ros
{
namespace arm_service
{
constexpr std::chrono::milliseconds default_wait_server_ms(1);

ManipulatorControlClient::ManipulatorControlClient(const rclcpp::NodeOptions& options)
  : Node("manipulator_control_client", options)
{
  this->set_callback_group_ =
      this->create_callback_group(rclcpp::CallbackGroupType::MutuallyExclusive);
  this->get_callback_group_ = this->create_callback_group(rclcpp::CallbackGroupType::Reentrant);

  this->get_control_mode_client_ = this->create_client<SrvManipulatorGetControlMode>(
      this->get_control_mode_srv_name_, rmw_qos_profile_services_default,
      this->get_callback_group_);
  this->set_control_mode_client_ = this->create_client<SrvManipulatorSetControlMode>(
      this->set_control_mode_srv_name_, rmw_qos_profile_services_default,
      this->set_callback_group_);
  this->move_joint_pose_client_ = this->create_client<SrvManipulatorMoveJointPose>(
      this->move_joint_pose_srv_name_, rmw_qos_profile_services_default, this->set_callback_group_);
  this->get_joint_pose_client_ = this->create_client<SrvManipulatorGetJointPose>(
      this->get_joint_pose_srv_name_, rmw_qos_profile_services_default, this->get_callback_group_);
  this->move_tcp_pose_client_ = this->create_client<SrvManipulatorMoveTcpPose>(
      this->move_tcp_pose_srv_name_, rmw_qos_profile_services_default, this->set_callback_group_);
  this->get_tcp_pose_client_ = this->create_client<SrvManipulatorGetTcpPose>(
      this->get_tcp_pose_srv_name_, rmw_qos_profile_services_default, this->get_callback_group_);
  this->target_reachable_client_ = this->create_client<SrvManipulatorTargetReachable>(
      this->target_reachable_srv_name_, rmw_qos_profile_services_default,
      this->get_callback_group_);
  this->claw_control_client_ = this->create_client<SrvManipulatorClawControl>(
      this->claw_control_srv_name_, rmw_qos_profile_services_default, this->set_callback_group_);
  this->get_claw_status_client_ = this->create_client<SrvManipulatorClawGetStatus>(
      this->claw_get_status_srv_name_, rmw_qos_profile_services_default, this->get_callback_group_);

  qrb::arm_manager::ManipulatorControlCallback manipulator_control_callback;
  manipulator_control_callback.get_control_mode_callback =
      std::bind(&ManipulatorControlClient::get_control_mode_func_, this, _1, _2);
  manipulator_control_callback.set_control_mode_callback =
      std::bind(&ManipulatorControlClient::set_control_mode_func_, this, _1, _2);
  manipulator_control_callback.get_joint_pose_callback =
      std::bind(&ManipulatorControlClient::get_joint_pose_func_, this, _1, _2);
  manipulator_control_callback.move_joint_pose_callback =
      std::bind(&ManipulatorControlClient::move_joint_pose_func_, this, _1, _2);
  manipulator_control_callback.get_tcp_pose_callback =
      std::bind(&ManipulatorControlClient::get_tcp_pose_func_, this, _1, _2);
  manipulator_control_callback.move_tcp_pose_callback =
      std::bind(&ManipulatorControlClient::move_tcp_pose_func_, this, _1, _2);
  manipulator_control_callback.check_target_reachable_callback =
      std::bind(&ManipulatorControlClient::check_target_reachable_func_, this, _1, _2);
  manipulator_control_callback.get_claw_status_callback =
      std::bind(&ManipulatorControlClient::get_claw_status_func_, this, _1, _2);
  manipulator_control_callback.control_claw_callback =
      std::bind(&ManipulatorControlClient::control_claw_func_, this, _1, _2);

  qrb::arm_manager::ArmManager& arm_manager_instance = qrb::arm_manager::ArmManager::get_instance();
  arm_manager_instance.register_manipulator_control_callback(manipulator_control_callback);
}

bool ManipulatorControlClient::get_control_mode_func_(
    qrb::arm_manager::ManipulatorGetControlMode& data, std::chrono::milliseconds wait_ms)
{
  if (!this->get_control_mode_client_->wait_for_service(default_wait_server_ms)) {
    RCLCPP_ERROR(this->get_logger(), "waiting for the service timeout.");
    return false;
  }

  RCLCPP_INFO(this->get_logger(), "service ready");

  // send request and wait result
  auto request = std::make_unique<SrvManipulatorGetControlMode::Request>();
  request->manipulator_id = data.manipulator_id;

  auto future = this->get_control_mode_client_->async_send_request(std::move(request));
  auto status = future.wait_for(wait_ms);
  if (status != std::future_status::ready) {
    RCLCPP_INFO(this->get_logger(), "wait service response timeout");
    return false;
  }

  auto response = future.get();
  RCLCPP_INFO_STREAM(this->get_logger(), "get control mode: " << response->result);
  data.mode = response->result;

  return true;
}

bool ManipulatorControlClient::set_control_mode_func_(
    qrb::arm_manager::ManipulatorSetControlMode& data, std::chrono::milliseconds wait_ms)
{
  if (!this->set_control_mode_client_->wait_for_service(default_wait_server_ms)) {
    RCLCPP_ERROR(this->get_logger(), "waiting for the service timeout.");
    return false;
  }

  RCLCPP_INFO(this->get_logger(), "service ready");

  // send request and wait result
  auto request = std::make_unique<SrvManipulatorSetControlMode::Request>();
  request->manipulator_id = data.manipulator_id;
  request->mode = data.mode;

  auto future = this->set_control_mode_client_->async_send_request(std::move(request));
  auto status = future.wait_for(wait_ms);
  if (status != std::future_status::ready) {
    RCLCPP_INFO(this->get_logger(), "wait service response timeout");
    return false;
  }

  auto response = future.get();
  RCLCPP_INFO_STREAM(this->get_logger(), "set control mode: " << response->result);
  data.result = response->result;

  return true;
}

bool ManipulatorControlClient::get_joint_pose_func_(qrb::arm_manager::ManipulatorGetJointPose& data,
                                                    std::chrono::milliseconds wait_ms)
{
  if (!this->get_joint_pose_client_->wait_for_service(default_wait_server_ms)) {
    RCLCPP_ERROR(this->get_logger(), "waiting for the service timeout.");
    return false;
  }

  RCLCPP_INFO(this->get_logger(), "service ready");

  // send request and wait result
  auto request = std::make_unique<SrvManipulatorGetJointPose::Request>();
  request->manipulator_id = data.manipulator_id;

  auto future = this->get_joint_pose_client_->async_send_request(std::move(request));
  auto status = future.wait_for(wait_ms);
  if (status != std::future_status::ready) {
    RCLCPP_INFO(this->get_logger(), "wait service response timeout");
    return false;
  }

  auto response = future.get();
  RCLCPP_INFO_STREAM(this->get_logger(), "get joint pose: " << response->result);
  data.joints_pose = std::move(response->joints_pose);
  data.joints_num = response->joints_num;
  data.result = response->result;

  return true;
}

bool ManipulatorControlClient::move_joint_pose_func_(
    qrb::arm_manager::ManipulatorMoveJointPose& data, std::chrono::milliseconds wait_ms)
{
  if (!this->move_joint_pose_client_->wait_for_service(default_wait_server_ms)) {
    RCLCPP_ERROR(this->get_logger(), "waiting for the service timeout.");
    return false;
  }

  RCLCPP_INFO(this->get_logger(), "service ready");

  // send request and wait result
  auto request = std::make_unique<SrvManipulatorMoveJointPose::Request>();
  request->manipulator_id = data.manipulator_id;
  request->speed = data.control_params.speed;
  request->acc = data.control_params.accleration;
  request->time = data.control_params.time;
  request->radius = data.control_params.radius;
  request->joints_pose = std::move(data.joints_pose);
  request->joints_num = data.joints_num;

  auto future = this->move_joint_pose_client_->async_send_request(std::move(request));
  auto status = future.wait_for(wait_ms);
  if (status != std::future_status::ready) {
    RCLCPP_INFO(this->get_logger(), "wait service response timeout");
    return false;
  }

  auto response = future.get();
  RCLCPP_INFO_STREAM(this->get_logger(), "move joint pose: " << response->result);
  data.result = response->result;

  return true;
}

bool ManipulatorControlClient::get_tcp_pose_func_(qrb::arm_manager::ManipulatorGetTcpPose& data,
                                                  std::chrono::milliseconds wait_ms)
{
  if (!this->get_tcp_pose_client_->wait_for_service(default_wait_server_ms)) {
    RCLCPP_ERROR(this->get_logger(), "waiting for the service timeout.");
    return false;
  }

  RCLCPP_INFO(this->get_logger(), "service ready");

  // send request and wait result
  auto request = std::make_unique<SrvManipulatorGetTcpPose::Request>();
  request->manipulator_id = data.manipulator_id;

  auto future = this->get_tcp_pose_client_->async_send_request(std::move(request));
  auto status = future.wait_for(wait_ms);
  if (status != std::future_status::ready) {
    RCLCPP_INFO(this->get_logger(), "wait service response timeout");
    return false;
  }

  auto response = future.get();
  RCLCPP_INFO_STREAM(this->get_logger(), "get tcp pose: " << response->result);
  data.tcp_pose.position_x = response->pose.position.x;
  data.tcp_pose.position_y = response->pose.position.y;
  data.tcp_pose.position_z = response->pose.position.z;
  data.tcp_pose.orientation_x = response->pose.orientation.x;
  data.tcp_pose.orientation_y = response->pose.orientation.y;
  data.tcp_pose.orientation_z = response->pose.orientation.z;
  data.tcp_pose.orientation_w = response->pose.orientation.w;
  data.result = response->result;

  return true;
}
bool ManipulatorControlClient::move_tcp_pose_func_(qrb::arm_manager::ManipulatorMoveTcpPose& data,
                                                   std::chrono::milliseconds wait_ms)
{
  if (!this->move_tcp_pose_client_->wait_for_service(default_wait_server_ms)) {
    RCLCPP_ERROR(this->get_logger(), "waiting for the service timeout.");
    return false;
  }

  RCLCPP_INFO(this->get_logger(), "service ready");

  // send request and wait result
  auto request = std::make_unique<SrvManipulatorMoveTcpPose::Request>();
  request->manipulator_id = data.manipulator_id;
  request->speed = data.control_params.speed;
  request->acc = data.control_params.accleration;
  request->time = data.control_params.time;
  request->radius = data.control_params.radius;
  request->pose.position.x = data.tcp_pose.position_x;
  request->pose.position.y = data.tcp_pose.position_y;
  request->pose.position.z = data.tcp_pose.position_z;
  request->pose.orientation.x = data.tcp_pose.orientation_x;
  request->pose.orientation.y = data.tcp_pose.orientation_y;
  request->pose.orientation.z = data.tcp_pose.orientation_z;
  request->pose.orientation.w = data.tcp_pose.orientation_w;

  auto future = this->move_tcp_pose_client_->async_send_request(std::move(request));
  auto status = future.wait_for(wait_ms);
  if (status != std::future_status::ready) {
    RCLCPP_INFO(this->get_logger(), "wait service response timeout");
    return false;
  }

  auto response = future.get();
  RCLCPP_INFO_STREAM(this->get_logger(), "move tcp pose: " << response->result);
  data.result = response->result;

  return true;
}
bool ManipulatorControlClient::check_target_reachable_func_(
    qrb::arm_manager::ManipulatorTargetReachable& data, std::chrono::milliseconds wait_ms)
{
  if (!this->target_reachable_client_->wait_for_service(default_wait_server_ms)) {
    RCLCPP_ERROR(this->get_logger(), "waiting for the service timeout.");
    return false;
  }

  RCLCPP_INFO(this->get_logger(), "service ready");

  // send request and wait result
  auto request = std::make_unique<SrvManipulatorTargetReachable::Request>();
  request->manipulator_id = data.manipulator_id;
  request->pose.position.x = data.tcp_pose.position_x;
  request->pose.position.y = data.tcp_pose.position_y;
  request->pose.position.z = data.tcp_pose.position_z;
  request->pose.orientation.x = data.tcp_pose.orientation_x;
  request->pose.orientation.y = data.tcp_pose.orientation_y;
  request->pose.orientation.z = data.tcp_pose.orientation_z;
  request->pose.orientation.w = data.tcp_pose.orientation_w;

  auto future = this->target_reachable_client_->async_send_request(std::move(request));
  auto status = future.wait_for(wait_ms);
  if (status != std::future_status::ready) {
    RCLCPP_INFO(this->get_logger(), "wait service response timeout");
    return false;
  }

  auto response = future.get();
  RCLCPP_INFO_STREAM(this->get_logger(), "check target reachable: " << response->result);
  data.result = response->result;

  return true;
}
bool ManipulatorControlClient::get_claw_status_func_(
    qrb::arm_manager::ManipulatorClawGetStatus& data, std::chrono::milliseconds wait_ms)
{
  if (!this->get_claw_status_client_->wait_for_service(default_wait_server_ms)) {
    RCLCPP_ERROR(this->get_logger(), "waiting for the service timeout.");
    return false;
  }

  RCLCPP_INFO(this->get_logger(), "service ready");

  // send request and wait result
  auto request = std::make_unique<SrvManipulatorClawGetStatus::Request>();
  request->manipulator_id = data.manipulator_id;

  auto future = this->get_claw_status_client_->async_send_request(std::move(request));
  auto status = future.wait_for(wait_ms);
  if (status != std::future_status::ready) {
    RCLCPP_INFO(this->get_logger(), "wait service response timeout");
    return false;
  }

  auto response = future.get();
  RCLCPP_INFO_STREAM(this->get_logger(), "get claw status: " << response->result);
  data.amplitude = std::move(response->amplitude);
  data.force = std::move(response->force);
  data.result = response->result;

  return true;
}
bool ManipulatorControlClient::control_claw_func_(qrb::arm_manager::ManipulatorClawControl& data,
                                                  std::chrono::milliseconds wait_ms)
{
  if (!this->claw_control_client_->wait_for_service(default_wait_server_ms)) {
    RCLCPP_ERROR(this->get_logger(), "waiting for the service timeout.");
    return false;
  }

  RCLCPP_INFO(this->get_logger(), "service ready");

  // send request and wait result
  auto request = std::make_unique<SrvManipulatorClawControl::Request>();
  request->manipulator_id = data.manipulator_id;
  request->amplitude = std::move(data.amplitude);
  request->force = std::move(data.force);

  auto future = this->claw_control_client_->async_send_request(std::move(request));
  auto status = future.wait_for(wait_ms);
  if (status != std::future_status::ready) {
    RCLCPP_INFO(this->get_logger(), "wait service response timeout");
    return false;
  }

  auto response = future.get();
  RCLCPP_INFO_STREAM(this->get_logger(), "control claw: " << response->result);
  data.result = response->result;

  return true;
}
}  // namespace arm_service
}  // namespace qrb_ros

#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(qrb_ros::arm_service::ManipulatorControlClient)
