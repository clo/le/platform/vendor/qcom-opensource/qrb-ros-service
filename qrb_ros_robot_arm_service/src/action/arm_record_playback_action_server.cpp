// Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#include "action/arm_record_playback_action_server.hpp"

using namespace std::placeholders;

namespace qrb_ros
{
namespace arm_service
{
ArmRecordPlaybackActionServer::ArmRecordPlaybackActionServer(const rclcpp::NodeOptions & options)
  : Node("arm_service_record_playback_action_server", options)
{
  this->callback_group_ = this->create_callback_group(rclcpp::CallbackGroupType::MutuallyExclusive);
  rclcpp::SubscriptionOptions sub_options;
  sub_options.callback_group = this->callback_group_;

  this->arm_record_server_ =
      rclcpp_action::create_server<ArmServiceRecord>(this, this->arm_record_action_name_,
          std::bind(&ArmRecordPlaybackActionServer::arm_record_handle_goal_, this, _1, _2),
          std::bind(&ArmRecordPlaybackActionServer::arm_record_handle_cancel_, this, _1),
          std::bind(&ArmRecordPlaybackActionServer::arm_record_handle_accepted_, this, _1),
          rcl_action_server_get_default_options(), this->callback_group_);

  this->arm_playback_server_ =
      rclcpp_action::create_server<ArmServicePlayback>(this, this->arm_playback_action_name_,
          std::bind(&ArmRecordPlaybackActionServer::arm_playback_handle_goal_, this, _1, _2),
          std::bind(&ArmRecordPlaybackActionServer::arm_playback_handle_cancel_, this, _1),
          std::bind(&ArmRecordPlaybackActionServer::arm_playback_handle_accepted_, this, _1),
          rcl_action_server_get_default_options(), this->callback_group_);

  qrb::arm_manager::ArmManager & arm_manager_instance =
      qrb::arm_manager::ArmManager::get_instance();
  this->arm_record_playback_instance_ = arm_manager_instance.get_arm_record_playback_instance();
}

rclcpp_action::GoalResponse ArmRecordPlaybackActionServer::arm_record_handle_goal_(
    const rclcpp_action::GoalUUID &,
    std::shared_ptr<const ArmServiceRecord::Goal> goal)
{
  RCLCPP_INFO_STREAM(this->get_logger(),
      "received goal request with manipulator_id: "
          << goal->manipulator_id << ", command: " << goal->command
          << ", record_id: " << (int)goal->record_id << ", sample rate: " << goal->sample_rate);
  if (goal->command == ArmServiceRecord::Goal::CMD_TEACHING_RECORD) {
    this->record_cmd_ = (uint8_t)qrb::arm_manager::ArmRecordCommand::CMD_TEACHING_RECORD;
  } else if (goal->command == ArmServiceRecord::Goal::CMD_CONTROLLING_RECORD) {
    this->record_cmd_ = (uint8_t)qrb::arm_manager::ArmRecordCommand::CMD_CONTROLLING_RECORD;
  } else {
    RCLCPP_WARN(this->get_logger(), "invalid record command");
    return rclcpp_action::GoalResponse::REJECT;
  }

  return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
}

rclcpp_action::CancelResponse ArmRecordPlaybackActionServer::arm_record_handle_cancel_(
    const std::shared_ptr<GoalHandleArmServiceRecord> goal_handle)
{
  RCLCPP_INFO(this->get_logger(), "received request to cancel goal");
  qrb::arm_manager::ActionUuid uuid{ goal_handle->get_goal_id() };
  this->arm_record_playback_instance_->stop_record_playback(uuid);
  return rclcpp_action::CancelResponse::ACCEPT;
}

void ArmRecordPlaybackActionServer::arm_record_handle_accepted_(
    const std::shared_ptr<GoalHandleArmServiceRecord> goal_handle)
{
  auto callback = std::make_shared<qrb::arm_manager::ArmRecordPlaybackActionCallback>();
  callback->publish_feedback = std::bind(
      &ArmRecordPlaybackActionServer::arm_record_publish_feedback_, this, goal_handle, _1);
  callback->publish_result =
      std::bind(&ArmRecordPlaybackActionServer::arm_record_publish_result_, this, goal_handle, _1);
  qrb::arm_manager::ActionUuid uuid{ goal_handle->get_goal_id() };
  auto arg = std::make_shared<qrb::arm_manager::StartArmRecordArg>();
  *arg = { this->record_cmd_, uuid, goal_handle->get_goal()->record_id,
    goal_handle->get_goal()->sample_rate, callback };
  this->arm_record_playback_instance_->start_arm_record(arg);
}

rclcpp_action::GoalResponse ArmRecordPlaybackActionServer::arm_playback_handle_goal_(
    const rclcpp_action::GoalUUID &,
    std::shared_ptr<const ArmServicePlayback::Goal> goal)
{
  RCLCPP_INFO_STREAM(this->get_logger(),
      "received goal request with manipulator_id: " << goal->manipulator_id
                                                    << ", record_id: " << goal->record_id);
  return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
}

rclcpp_action::CancelResponse ArmRecordPlaybackActionServer::arm_playback_handle_cancel_(
    const std::shared_ptr<GoalHandleArmServicePlayback> goal_handle)
{
  RCLCPP_INFO(this->get_logger(), "received request to cancel goal");
  qrb::arm_manager::ActionUuid uuid{ goal_handle->get_goal_id() };
  this->arm_record_playback_instance_->stop_record_playback(uuid);
  return rclcpp_action::CancelResponse::ACCEPT;
}
void ArmRecordPlaybackActionServer::arm_playback_handle_accepted_(
    const std::shared_ptr<GoalHandleArmServicePlayback> goal_handle)
{
  auto callback = std::make_shared<qrb::arm_manager::ArmRecordPlaybackActionCallback>();
  callback->publish_feedback = std::bind(
      &ArmRecordPlaybackActionServer::arm_playback_publish_feedback_, this, goal_handle, _1);
  callback->publish_result = std::bind(
      &ArmRecordPlaybackActionServer::arm_playback_publish_result_, this, goal_handle, _1);
  qrb::arm_manager::ActionUuid uuid{ goal_handle->get_goal_id() };

  auto arg = std::make_shared<qrb::arm_manager::StartArmPlaybackArg>();
  *arg = { uuid, goal_handle->get_goal()->record_id, callback };
  this->arm_record_playback_instance_->start_arm_playback(arg);
}

bool ArmRecordPlaybackActionServer::arm_record_publish_feedback_(
    const std::shared_ptr<GoalHandleArmServiceRecord> goal_handle,
    qrb::arm_manager::ArmRecordPlaybackFeedback data)
{
  if (rclcpp::ok()) {
    auto feedback = std::make_unique<ArmServiceRecord::Feedback>();
    feedback->elapsed_time.sec = data.elapsed_time.count() / 1000;
    feedback->elapsed_time.nanosec = (data.elapsed_time.count() % 1000) * 1000 * 1000;
    goal_handle->publish_feedback(std::move(feedback));
    RCLCPP_INFO_STREAM(
        this->get_logger(), "publish feedback: time: " << data.elapsed_time.count() << "ms");
    return true;
  }
  return false;
}

bool ArmRecordPlaybackActionServer::arm_record_publish_result_(
    const std::shared_ptr<GoalHandleArmServiceRecord> goal_handle,
    bool result)
{
  if (rclcpp::ok()) {
    auto action_result = std::make_shared<ArmServiceRecord::Result>();
    action_result->result = result;
    if (goal_handle->is_canceling()) {
      goal_handle->canceled(action_result);
      RCLCPP_INFO(this->get_logger(), "goal canceled");
    } else {
      goal_handle->succeed(action_result);
      if (result) {
        RCLCPP_INFO(this->get_logger(), "goal succeeded");
      } else {
        RCLCPP_INFO(this->get_logger(), "goal failed");
      }
    }

    return true;
  }
  return false;
}

bool ArmRecordPlaybackActionServer::arm_playback_publish_feedback_(
    const std::shared_ptr<GoalHandleArmServicePlayback> goal_handle,
    qrb::arm_manager::ArmRecordPlaybackFeedback data)
{
  if (rclcpp::ok()) {
    auto feedback = std::make_unique<ArmServicePlayback::Feedback>();
    feedback->elapsed_time.sec = data.elapsed_time.count() / 1000;
    feedback->elapsed_time.nanosec = (data.elapsed_time.count() % 1000) * 1000 * 1000;
    goal_handle->publish_feedback(std::move(feedback));
    RCLCPP_INFO_STREAM(
        this->get_logger(), "publish feedback: time: " << data.elapsed_time.count() << "ms");
    return true;
  }
  return false;
}
bool ArmRecordPlaybackActionServer::arm_playback_publish_result_(
    const std::shared_ptr<GoalHandleArmServicePlayback> goal_handle,
    bool result)
{
  if (rclcpp::ok()) {
    auto action_result = std::make_shared<ArmServicePlayback::Result>();
    action_result->result = result;
    if (goal_handle->is_canceling()) {
      goal_handle->canceled(action_result);
      RCLCPP_INFO(this->get_logger(), "goal canceled");
    } else {
      goal_handle->succeed(action_result);
      if (result) {
        RCLCPP_INFO(this->get_logger(), "goal succeeded");
      } else {
        RCLCPP_INFO(this->get_logger(), "goal failed");
      }
    }

    return true;
  }
  return false;
}

}  // namespace arm_service
}  // namespace qrb_ros

#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(qrb_ros::arm_service::ArmRecordPlaybackActionServer)