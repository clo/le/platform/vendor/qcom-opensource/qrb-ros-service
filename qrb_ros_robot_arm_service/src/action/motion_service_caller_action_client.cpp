// Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#include "action/motion_service_caller_action_client.hpp"

namespace qrb_ros
{
namespace arm_service
{
using VelocityLevel = qrb_ros_motion_msgs::msg::VelocityLevel;
constexpr std::chrono::milliseconds default_motion_server_wait_ms(100);
constexpr std::chrono::seconds default_motion_action_wait_s(20);

MotionServiceCallerActionClient::MotionServiceCallerActionClient(
    const rclcpp::NodeOptions & options)
  : Node("motion_service_caller_action_client", options)
{
  this->callback_group_ = this->create_callback_group(rclcpp::CallbackGroupType::MutuallyExclusive);
  rclcpp::SubscriptionOptions sub_options;
  sub_options.callback_group = this->callback_group_;

  this->line_motion_client_ = rclcpp_action::create_client<LineMotion>(
      this, this->line_motion_name_, this->callback_group_);
  this->rotation_motion_client_ = rclcpp_action::create_client<RotationMotion>(
      this, this->rotation_motion_name_, this->callback_group_);

  this->node_ = this->get_node_base_interface();

  auto callback =
      std::bind(&MotionServiceCallerActionClient::send_motion_goal_, this, std::placeholders::_1);
  qrb::arm_manager::ArmManager & arm_manager_instance =
      qrb::arm_manager::ArmManager::get_instance();
  arm_manager_instance.register_amr_motion_controller_callback(callback);
}

bool MotionServiceCallerActionClient::send_line_motion_goal_(LineMotion::Goal & goal)
{
  if (!this->line_motion_client_->wait_for_action_server(default_motion_server_wait_ms)) {
    RCLCPP_ERROR(this->get_logger(), "line motion action server not available after waiting");
    return false;
  }

  RCLCPP_INFO(this->get_logger(), "sending goal");

  auto goal_handle_future = this->line_motion_client_->async_send_goal(goal);
  auto goal_status = goal_handle_future.wait_for(default_motion_server_wait_ms);
  if (goal_status != std::future_status::ready) {
    RCLCPP_INFO(this->get_logger(), "wait motion goal handler response timeout");
    return false;
  }

  auto goal_handle = goal_handle_future.get();
  if (!goal_handle) {
    RCLCPP_ERROR(this->get_logger(), "goal was rejected by server");
    return false;
  }

  auto result_future = this->line_motion_client_->async_get_result(goal_handle);
  RCLCPP_INFO(this->get_logger(), "waiting for result");
  if (result_future.wait_for(default_motion_action_wait_s) != std::future_status::ready) {
    RCLCPP_WARN(this->get_logger(), "waiting line motion goal timeout.");
    // cancel the goal
    auto cancel_result_future = this->line_motion_client_->async_cancel_goal(goal_handle);
    if (cancel_result_future.wait_for(std::chrono::seconds(5)) != std::future_status::ready) {
      RCLCPP_WARN(this->get_logger(), "cancel goal failed.");
    } else {
      RCLCPP_INFO(this->get_logger(), "goal canceled.");
    }
    return false;
  }

  auto wrapped_result = result_future.get();
  switch (wrapped_result.code) {
    case rclcpp_action::ResultCode::SUCCEEDED:
      RCLCPP_INFO_STREAM(
          this->get_logger(), "line motion action result: " << wrapped_result.result->result);
      return wrapped_result.result->result;
    case rclcpp_action::ResultCode::ABORTED:
      RCLCPP_ERROR(this->get_logger(), "goal was aborted");
      return false;
    case rclcpp_action::ResultCode::CANCELED:
      RCLCPP_ERROR(this->get_logger(), "goal was canceled");
      return false;
    default:
      RCLCPP_ERROR(this->get_logger(), "invalid result code");
      return false;
  }
}

bool MotionServiceCallerActionClient::send_rotation_motion_goal_(RotationMotion::Goal & goal)
{
  if (!this->rotation_motion_client_->wait_for_action_server(default_motion_server_wait_ms)) {
    RCLCPP_ERROR(this->get_logger(), "rotation motion action server not available after waiting");
    return false;
  }

  RCLCPP_INFO(this->get_logger(), "sending goal");

  auto goal_handle_future = this->rotation_motion_client_->async_send_goal(goal);
  auto goal_status = goal_handle_future.wait_for(default_motion_server_wait_ms);
  if (goal_status != std::future_status::ready) {
    RCLCPP_INFO(this->get_logger(), "wait motion goal handler response timeout");
    return false;
  }

  auto goal_handle = goal_handle_future.get();
  if (!goal_handle) {
    RCLCPP_ERROR(this->get_logger(), "goal was rejected by server");
    return false;
  }

  auto result_future = this->rotation_motion_client_->async_get_result(goal_handle);
  RCLCPP_INFO(this->get_logger(), "waiting for result");
  if (result_future.wait_for(default_motion_action_wait_s) != std::future_status::ready) {
    RCLCPP_WARN(this->get_logger(), "waiting rotaion motion goal timeout.");
    // cancel the goal
    auto cancel_result_future = this->rotation_motion_client_->async_cancel_goal(goal_handle);
    if (cancel_result_future.wait_for(std::chrono::seconds(5)) != std::future_status::ready) {
      RCLCPP_WARN(this->get_logger(), "cancel goal failed.");
    } else {
      RCLCPP_INFO(this->get_logger(), "goal canceled.");
    }
    return false;
  }

  auto wrapped_result = result_future.get();
  switch (wrapped_result.code) {
    case rclcpp_action::ResultCode::SUCCEEDED:
      RCLCPP_INFO_STREAM(
          this->get_logger(), "line motion action result: " << wrapped_result.result->result);
      return wrapped_result.result->result;
    case rclcpp_action::ResultCode::ABORTED:
      RCLCPP_ERROR(this->get_logger(), "goal was aborted");
      return false;
    case rclcpp_action::ResultCode::CANCELED:
      RCLCPP_ERROR(this->get_logger(), "goal was canceled");
      return false;
    default:
      RCLCPP_ERROR(this->get_logger(), "invalid result code");
      return false;
  }
}

bool MotionServiceCallerActionClient::send_motion_goal_(const qrb::arm_manager::AmrMotionMsg & data)
{
  VelocityLevel velocity_level;
  switch (data.velocity_level) {
    case qrb::arm_manager::MOTION_VELOCITY_LOW:
      velocity_level.value = VelocityLevel::SLOW_SPEED;
      break;
    case qrb::arm_manager::MOTION_VELOCITY_MEDIUM:
      velocity_level.value = VelocityLevel::MEDIUM_SPEED;
      break;
    case qrb::arm_manager::MOTION_VELOCITY_FAST:
      velocity_level.value = VelocityLevel::FAST_SPEED;
      break;
    default:
      RCLCPP_ERROR(this->get_logger(), "invalid velocity level");
      return false;
  }

  if (data.type == qrb::arm_manager::MOTION_TYPE_LINE) {
    LineMotion::Goal goal;
    goal.velocity = velocity_level;
    goal.distance = data.value;
    return this->send_line_motion_goal_(goal);
  } else if (data.type == qrb::arm_manager::MOTION_TYPE_ROTATION) {
    RotationMotion::Goal goal;
    goal.velocity = velocity_level;
    goal.angle = data.value;
    return this->send_rotation_motion_goal_(goal);
  } else {
    RCLCPP_ERROR(this->get_logger(), "invalid motion type");
    return false;
  }
}
}  // namespace arm_service
}  // namespace qrb_ros

#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(qrb_ros::arm_service::MotionServiceCallerActionClient)
