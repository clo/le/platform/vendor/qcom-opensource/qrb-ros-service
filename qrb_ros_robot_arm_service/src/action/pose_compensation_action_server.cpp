// Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#include "action/pose_compensation_action_server.hpp"

using namespace std::placeholders;

namespace qrb_ros
{
namespace arm_service
{
PoseCompensationActionServer::PoseCompensationActionServer(const rclcpp::NodeOptions & options)
  : Node("arm_service_pose_compensation_action_server", options)
{
  this->callback_group_ = this->create_callback_group(rclcpp::CallbackGroupType::MutuallyExclusive);
  rclcpp::SubscriptionOptions sub_options;
  sub_options.callback_group = this->callback_group_;

  this->pose_compensation_server_ = rclcpp_action::create_server<PoseCompensationAction>(this,
      this->pose_compensation_action_name_,
      std::bind(&PoseCompensationActionServer::pose_compensation_handle_goal_, this, _1, _2),
      std::bind(&PoseCompensationActionServer::pose_compensation_handle_cancel_, this, _1),
      std::bind(&PoseCompensationActionServer::pose_compensation_handle_accepted_, this, _1),
      rcl_action_server_get_default_options(), this->callback_group_);

  qrb::arm_manager::ArmManager & arm_manager_instance =
      qrb::arm_manager::ArmManager::get_instance();
  this->pose_compensation_instance_ = arm_manager_instance.get_pose_compensation_instance();
}

rclcpp_action::GoalResponse PoseCompensationActionServer::pose_compensation_handle_goal_(
    const rclcpp_action::GoalUUID &,
    std::shared_ptr<const PoseCompensationAction::Goal> goal)
{
  RCLCPP_INFO_STREAM(this->get_logger(),
      "received goal request with manipulator_id: " << goal->manipulator_id
                                                    << ", pose_id: " << (int)goal->pose_id);
  return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
}

rclcpp_action::CancelResponse PoseCompensationActionServer::pose_compensation_handle_cancel_(
    const std::shared_ptr<GoalHandlePoseCompensationAction> goal_handle)
{
  RCLCPP_INFO(this->get_logger(), "received request to cancel goal");
  qrb::arm_manager::ActionUuid uuid{ goal_handle->get_goal_id() };
  this->pose_compensation_instance_->stop_pose_compensation(uuid);
  return rclcpp_action::CancelResponse::ACCEPT;
}

void PoseCompensationActionServer::pose_compensation_handle_accepted_(
    const std::shared_ptr<GoalHandlePoseCompensationAction> goal_handle)
{
  qrb::arm_manager::ActionUuid uuid{ goal_handle->get_goal_id() };

  auto action_callback = std::make_shared<qrb::arm_manager::PoseCompensationActionCallback>();
  action_callback->publish_feedback = std::bind(
      &PoseCompensationActionServer::pose_compensation_publish_feedback_, this, goal_handle, _1);
  action_callback->publish_result = std::bind(
      &PoseCompensationActionServer::pose_compensation_publish_result_, this, goal_handle, _1);

  auto arg = std::make_shared<qrb::arm_manager::StartPoseCompensationArg>();
  *arg = { uuid, goal_handle->get_goal()->pose_id, action_callback };
  this->pose_compensation_instance_->start_pose_compensation(arg);
}

bool PoseCompensationActionServer::pose_compensation_publish_feedback_(
    const std::shared_ptr<GoalHandlePoseCompensationAction> goal_handle,
    qrb::arm_manager::PoseCompensationFeedback data)
{
  if (rclcpp::ok()) {
    auto feedback = std::make_unique<PoseCompensationAction::Feedback>();
    feedback->state = (int)data.state;
    feedback->elapsed_time.sec = data.elapsed_time.count() / 1000;
    feedback->elapsed_time.nanosec = (data.elapsed_time.count() % 1000) * 1000 * 1000;
    goal_handle->publish_feedback(std::move(feedback));
    RCLCPP_INFO_STREAM(
        this->get_logger(), "publish feedback: time: " << data.elapsed_time.count() << "ms, "
                                                       << "state: " << (int)data.state);
    return true;
  }
  return false;
}

bool PoseCompensationActionServer::pose_compensation_publish_result_(
    const std::shared_ptr<GoalHandlePoseCompensationAction> goal_handle,
    bool result)
{
  if (rclcpp::ok()) {
    auto action_result = std::make_shared<PoseCompensationAction::Result>();
    action_result->result = result;
    if (goal_handle->is_canceling()) {
      goal_handle->canceled(action_result);
      RCLCPP_INFO(this->get_logger(), "goal canceled");
    } else {
      goal_handle->succeed(action_result);
      if (result) {
        RCLCPP_INFO(this->get_logger(), "goal succeeded");
      } else {
        RCLCPP_INFO(this->get_logger(), "goal failed");
      }
    }
    return true;
  }
  return false;
}

}  // namespace arm_service
}  // namespace qrb_ros

#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(qrb_ros::arm_service::PoseCompensationActionServer)