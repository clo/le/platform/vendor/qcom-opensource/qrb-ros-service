// Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#include "topic/system_status_subscriber.hpp"

using namespace std::placeholders;

namespace qrb_ros
{
namespace arm_service
{
constexpr uint8_t amr_status_id = 5;

SyetemStatusSubscriber::SyetemStatusSubscriber(const rclcpp::NodeOptions & options)
  : Node("arm_service_system_status_subscriber", options)
{
  RCLCPP_INFO(this->get_logger(), "topic sub start");

  this->manipulator_exception_sub_ = this->create_subscription<ManipulatorException>(
      this->manipulator_exception_name_, this->topic_queue_size_,
      std::bind(&SyetemStatusSubscriber::manipulator_exception_sub_cb_, this, _1));

  this->amr_status_sub_ = this->create_subscription<AMRStatus>(this->amr_status_name_,
      this->topic_queue_size_, std::bind(&SyetemStatusSubscriber::amr_status_sub_cb_, this, _1));

  qrb::arm_manager::ArmManager & arm_manager_instance =
      qrb::arm_manager::ArmManager::get_instance();
  this->system_status_monitor_instance_ = arm_manager_instance.get_system_status_monitor_instance();
}

void SyetemStatusSubscriber::manipulator_exception_sub_cb_(ManipulatorException::ConstSharedPtr msg)
{
  RCLCPP_INFO_STREAM(this->get_logger(), "received manipulator exception msg, manipulator id: "
                                             << msg->manipulator_id
                                             << ", error code: " << msg->error_code);
  if (msg->error_code != ManipulatorException::NORMAL) {
    qrb::arm_manager::ManipulatorException exception{ msg->manipulator_id, msg->error_code };
    this->system_status_monitor_instance_->handle_manipulator_exception(exception);
  }
}

void SyetemStatusSubscriber::amr_status_sub_cb_(AMRStatus::ConstSharedPtr msg)
{
  if (msg->status_change_id == amr_status_id) {
    RCLCPP_INFO_STREAM(this->get_logger(), "received amr status msg, status change id: "
                                               << (int)msg->status_change_id
                                               << ", current status: " << (int)msg->current_state);
    this->system_status_monitor_instance_->handle_amr_status(msg->current_state);
  }
}

}  // namespace arm_service
}  // namespace qrb_ros

#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(qrb_ros::arm_service::SyetemStatusSubscriber)
