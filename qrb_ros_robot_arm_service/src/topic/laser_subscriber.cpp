// Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear
#include "topic/laser_subscriber.hpp"

#include "rclcpp/wait_for_message.hpp"

namespace qrb_ros
{
namespace arm_service
{
constexpr float MAX_SCAN_RANGE = 2.5;
constexpr float MIN_SCAN_ANGLE = 100 / 180 * M_PI;
constexpr std::chrono::milliseconds default_laser_sub_wait_ms(200);

LaserSubscriber::LaserSubscriber(const rclcpp::NodeOptions & options)
  : Node("arm_service_laser_helper", options)
{
  this->laser_subscriber_node_ = std::make_shared<rclcpp::Node>("arm_service_laser_subscriber");

  auto callback = std::bind(&LaserSubscriber::subscribe_once_, this, std::placeholders::_1);
  qrb::arm_manager::ArmManager & arm_manager_instance =
      qrb::arm_manager::ArmManager::get_instance();
  arm_manager_instance.register_point_cloud_subscriber_callback(callback);
}

bool LaserSubscriber::subscribe_once_(pcl::PointCloud<pcl::PointXYZ> & point_cloud)
{
  RCLCPP_INFO(this->get_logger(), "subscribe to laser data once");
  sensor_msgs::msg::LaserScan scan_msg;

  if (!rclcpp::wait_for_message(scan_msg, this->laser_subscriber_node_,
          this->laser_subscriber_name_, default_laser_sub_wait_ms)) {
    RCLCPP_ERROR(this->get_logger(), "wait for laser msg failed.");
    return false;
  }

  int scan_count = scan_msg.ranges.size();
  for (int i = 0; i < scan_count; ++i) {
    if (!std::isfinite(scan_msg.ranges[i]) || scan_msg.ranges[i] > MAX_SCAN_RANGE) {
      continue;
    }
    double angle_each = scan_msg.angle_min + scan_msg.angle_increment * i;
    if (3.136 < angle_each && angle_each < 3.14) {
      RCLCPP_INFO_STREAM(this->get_logger(),
          "scan angle value: " << angle_each << ", scan distance: " << scan_msg.ranges[i]);
    }
    if (abs(angle_each) < MIN_SCAN_ANGLE) {
      continue;
    }

    pcl::PointXYZ point;
    point.x = scan_msg.ranges[i] * cos(angle_each);
    point.y = scan_msg.ranges[i] * sin(angle_each);
    point.z = 0;
    point_cloud.push_back(point);
  }

  return true;
}
}  // namespace arm_service
}  // namespace qrb_ros

#include "rclcpp_components/register_node_macro.hpp"
RCLCPP_COMPONENTS_REGISTER_NODE(qrb_ros::arm_service::LaserSubscriber)
