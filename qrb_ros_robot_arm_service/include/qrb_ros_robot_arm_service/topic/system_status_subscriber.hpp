// Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#ifndef QRB_ROS_ARM_SERVICE__SYSTEM_STATUS_SUBSCRIBER_HPP_
#define QRB_ROS_ARM_SERVICE__SYSTEM_STATUS_SUBSCRIBER_HPP_

#include "rclcpp/rclcpp.hpp"

#include "qrb_ros_manipulator_msgs/msg/manipulator_exception.hpp"
#include "qrb_ros_amr_msgs/msg/amr_status.hpp"
#include "qrb_robot_arm_manager/arm_manager_interface.hpp"

namespace qrb_ros
{
namespace arm_service
{
class SyetemStatusSubscriber : public rclcpp::Node
{
public:
  using ManipulatorException = qrb_ros_manipulator_msgs::msg::ManipulatorException;
  using AMRStatus = qrb_ros_amr_msgs::msg::AMRStatus;

  explicit SyetemStatusSubscriber(const rclcpp::NodeOptions& options);

private:
  rclcpp::Subscription<ManipulatorException>::SharedPtr manipulator_exception_sub_{ nullptr };
  void manipulator_exception_sub_cb_(ManipulatorException::ConstSharedPtr msg);

  rclcpp::Subscription<AMRStatus>::SharedPtr amr_status_sub_{ nullptr };
  void amr_status_sub_cb_(AMRStatus::ConstSharedPtr msg);

  const std::string manipulator_exception_name_ = "manipulator_exception";
  const std::string amr_status_name_ = "amr_status";
  const int topic_queue_size_ = 10;

  std::shared_ptr<qrb::arm_manager::SystemStatusMonitorBase> system_status_monitor_instance_{
    nullptr
  };
};
}  // namespace arm_service
}  // namespace qrb_ros
#endif  // QRB_ROS_ARM_SERVICE__SYSTEM_STATUS_SUBSCRIBER_HPP_