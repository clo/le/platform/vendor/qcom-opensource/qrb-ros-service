// Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#ifndef QRB_ROS_ARM_SERVICE__LASER_SUBSCRIBER_HPP_
#define QRB_ROS_ARM_SERVICE__LASER_SUBSCRIBER_HPP_

#include "rclcpp/rclcpp.hpp"
#include "sensor_msgs/msg/laser_scan.hpp"

#include "qrb_robot_arm_manager/arm_manager_interface.hpp"

namespace qrb_ros
{
namespace arm_service
{
class LaserSubscriber : public rclcpp::Node
{
public:
  explicit LaserSubscriber(const rclcpp::NodeOptions& options);

private:
  std::shared_ptr<rclcpp::Node> laser_subscriber_node_{ nullptr };
  bool subscribe_once_(pcl::PointCloud<pcl::PointXYZ>& point_cloud);

  const std::string laser_subscriber_name_ = "/scan";
};
}  // namespace arm_service
}  // namespace qrb_ros
#endif  // QRB_ROS_ARM_SERVICE__LASER_SUBSCRIBER_HPP_