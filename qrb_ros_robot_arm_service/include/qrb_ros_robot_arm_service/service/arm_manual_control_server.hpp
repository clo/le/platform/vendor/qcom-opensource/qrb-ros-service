// Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#ifndef QRB_ROS_ARM_SERVICE__ARM_MANUAL_CONTROL_SERVER_HPP_
#define QRB_ROS_ARM_SERVICE__ARM_MANUAL_CONTROL_SERVER_HPP_

#include "rclcpp/rclcpp.hpp"

#include "qrb_ros_manipulator_msgs/srv/manipulator_get_control_mode.hpp"
#include "qrb_ros_manipulator_msgs/srv/manipulator_set_control_mode.hpp"
#include "qrb_ros_manipulator_msgs/srv/manipulator_move_joint_pose.hpp"
#include "qrb_ros_manipulator_msgs/srv/manipulator_get_joint_pose.hpp"
#include "qrb_ros_manipulator_msgs/srv/manipulator_move_tcp_pose.hpp"
#include "qrb_ros_manipulator_msgs/srv/manipulator_get_tcp_pose.hpp"
#include "qrb_ros_manipulator_msgs/srv/manipulator_target_reachable.hpp"
#include "qrb_ros_manipulator_msgs/srv/manipulator_claw_control.hpp"
#include "qrb_ros_manipulator_msgs/srv/manipulator_claw_get_status.hpp"

#include "qrb_robot_arm_manager/arm_manager_interface.hpp"

namespace qrb_ros
{
namespace arm_service
{
class ArmManualControlServer : public rclcpp::Node
{
public:
  using SrvManipulatorGetControlMode = qrb_ros_manipulator_msgs::srv::ManipulatorGetControlMode;
  using SrvManipulatorSetControlMode = qrb_ros_manipulator_msgs::srv::ManipulatorSetControlMode;
  using SrvManipulatorMoveJointPose = qrb_ros_manipulator_msgs::srv::ManipulatorMoveJointPose;
  using SrvManipulatorGetJointPose = qrb_ros_manipulator_msgs::srv::ManipulatorGetJointPose;
  using SrvManipulatorMoveTcpPose = qrb_ros_manipulator_msgs::srv::ManipulatorMoveTcpPose;
  using SrvManipulatorGetTcpPose = qrb_ros_manipulator_msgs::srv::ManipulatorGetTcpPose;
  using SrvManipulatorTargetReachable = qrb_ros_manipulator_msgs::srv::ManipulatorTargetReachable;
  using SrvManipulatorClawControl = qrb_ros_manipulator_msgs::srv::ManipulatorClawControl;
  using SrvManipulatorClawGetStatus = qrb_ros_manipulator_msgs::srv::ManipulatorClawGetStatus;
  explicit ArmManualControlServer(const rclcpp::NodeOptions& options);

private:
  rclcpp::CallbackGroup::SharedPtr callback_group_{ nullptr };

  rclcpp::Service<SrvManipulatorGetControlMode>::SharedPtr get_control_mode_server_{ nullptr };
  rclcpp::Service<SrvManipulatorSetControlMode>::SharedPtr set_control_mode_server_{ nullptr };

  rclcpp::Service<SrvManipulatorMoveJointPose>::SharedPtr move_joint_pose_server_{ nullptr };
  rclcpp::Service<SrvManipulatorGetJointPose>::SharedPtr get_joint_pose_server_{ nullptr };

  rclcpp::Service<SrvManipulatorMoveTcpPose>::SharedPtr move_tcp_pose_server_{ nullptr };
  rclcpp::Service<SrvManipulatorGetTcpPose>::SharedPtr get_tcp_pose_server_{ nullptr };

  rclcpp::Service<SrvManipulatorTargetReachable>::SharedPtr target_reachable_server_{ nullptr };

  rclcpp::Service<SrvManipulatorClawControl>::SharedPtr claw_control_server_{ nullptr };
  rclcpp::Service<SrvManipulatorClawGetStatus>::SharedPtr get_claw_status_server_{ nullptr };

  const std::string get_control_mode_srv_name_ = "arm_get_control_mode";
  const std::string set_control_mode_srv_name_ = "arm_set_control_mode";
  const std::string move_joint_pose_srv_name_ = "arm_move_joint_pose";
  const std::string get_joint_pose_srv_name_ = "arm_get_joint_pose";
  const std::string move_tcp_pose_srv_name_ = "arm_move_tcp_pose";
  const std::string get_tcp_pose_srv_name_ = "arm_get_tcp_pose";
  const std::string target_reachable_srv_name_ = "arm_target_reachable";
  const std::string claw_control_srv_name_ = "arm_claw_control";
  const std::string claw_get_status_srv_name_ = "arm_claw_get_status";

  std::shared_ptr<qrb::arm_manager::ArmManualControlBase> arm_manual_control_instance_{ nullptr };

  void get_control_mode_cb_(const std::shared_ptr<rmw_request_id_t> request_header,
                            const std::shared_ptr<SrvManipulatorGetControlMode::Request> request,
                            std::shared_ptr<SrvManipulatorGetControlMode::Response> response);
  void set_control_mode_cb_(const std::shared_ptr<rmw_request_id_t> request_header,
                            const std::shared_ptr<SrvManipulatorSetControlMode::Request> request,
                            std::shared_ptr<SrvManipulatorSetControlMode::Response> response);
  void move_joint_pose_cb_(const std::shared_ptr<rmw_request_id_t> request_header,
                           const std::shared_ptr<SrvManipulatorMoveJointPose::Request> request,
                           std::shared_ptr<SrvManipulatorMoveJointPose::Response> response);
  void get_joint_pose_cb_(const std::shared_ptr<rmw_request_id_t> request_header,
                          const std::shared_ptr<SrvManipulatorGetJointPose::Request> request,
                          std::shared_ptr<SrvManipulatorGetJointPose::Response> response);
  void move_tcp_pose_cb_(const std::shared_ptr<rmw_request_id_t> request_header,
                         const std::shared_ptr<SrvManipulatorMoveTcpPose::Request> request,
                         std::shared_ptr<SrvManipulatorMoveTcpPose::Response> response);
  void get_tcp_pose_cb_(const std::shared_ptr<rmw_request_id_t> request_header,
                        const std::shared_ptr<SrvManipulatorGetTcpPose::Request> request,
                        std::shared_ptr<SrvManipulatorGetTcpPose::Response> response);
  void target_reachable_cb_(const std::shared_ptr<rmw_request_id_t> request_header,
                            const std::shared_ptr<SrvManipulatorTargetReachable::Request> request,
                            std::shared_ptr<SrvManipulatorTargetReachable::Response> response);
  void claw_control_cb_(const std::shared_ptr<rmw_request_id_t> request_header,
                        const std::shared_ptr<SrvManipulatorClawControl::Request> request,
                        std::shared_ptr<SrvManipulatorClawControl::Response> response);
  void get_claw_status_cb_(const std::shared_ptr<rmw_request_id_t> request_header,
                           const std::shared_ptr<SrvManipulatorClawGetStatus::Request> request,
                           std::shared_ptr<SrvManipulatorClawGetStatus::Response> response);
};

}  // namespace arm_service
}  // namespace qrb_ros
#endif  // QRB_ROS_ARM_SERVICE__ARM_MANUAL_CONTROL_SERVER_HPP_