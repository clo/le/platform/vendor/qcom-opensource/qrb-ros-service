// Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#ifndef QRB_ROS_ARM_SERVICE__POSE_COMPENSATION_FILE_SERVER_HPP_
#define QRB_ROS_ARM_SERVICE__POSE_COMPENSATION_FILE_SERVER_HPP_

#include "rclcpp/rclcpp.hpp"

#include "qrb_ros_robot_arm_service_msgs/srv/arm_service_check_file.hpp"
#include "qrb_ros_robot_arm_service_msgs/srv/arm_service_save_delete_file.hpp"

#include "qrb_robot_arm_manager/arm_manager_interface.hpp"

namespace qrb_ros
{
namespace arm_service
{
class PoseCompensationFileServer : public rclcpp::Node
{
public:
  using ArmServiceCheckFile = qrb_ros_robot_arm_service_msgs::srv::ArmServiceCheckFile;
  using ArmServiceSaveDeleteFile = qrb_ros_robot_arm_service_msgs::srv::ArmServiceSaveDeleteFile;

  explicit PoseCompensationFileServer(const rclcpp::NodeOptions& options);

private:
  rclcpp::CallbackGroup::SharedPtr callback_group_{ nullptr };
  rclcpp::Service<ArmServiceCheckFile>::SharedPtr check_file_server_{ nullptr };
  rclcpp::Service<ArmServiceSaveDeleteFile>::SharedPtr save_delete_file_server_{ nullptr };

  const std::string check_file_srv_name_ = "arm_check_pose_compensation_file";
  const std::string save_delete_file_srv_name_ = "arm_save_delete_pose_compensation_file";

  std::shared_ptr<qrb::arm_manager::PoseCompensationBase> pose_compensation_instance_{ nullptr };

  void check_file_cb_(const std::shared_ptr<rmw_request_id_t> request_header,
                      const std::shared_ptr<ArmServiceCheckFile::Request> request,
                      std::shared_ptr<ArmServiceCheckFile::Response> response);
  void save_delete_file_cb_(const std::shared_ptr<rmw_request_id_t> request_header,
                            const std::shared_ptr<ArmServiceSaveDeleteFile::Request> request,
                            std::shared_ptr<ArmServiceSaveDeleteFile::Response> response);
};
}  // namespace arm_service
}  // namespace qrb_ros
#endif  // QRB_ROS_ARM_SERVICE__POSE_COMPENSATION_FILE_SERVER_HPP_