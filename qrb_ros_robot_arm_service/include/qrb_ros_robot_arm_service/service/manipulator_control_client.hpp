// Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#ifndef QRB_ROS_ARM_SERVICE__MANIPULATOR_CONTROL_CLIENT_HPP_
#define QRB_ROS_ARM_SERVICE__MANIPULATOR_CONTROL_CLIENT_HPP_

#include "rclcpp/rclcpp.hpp"

#include "qrb_ros_manipulator_msgs/srv/manipulator_get_control_mode.hpp"
#include "qrb_ros_manipulator_msgs/srv/manipulator_set_control_mode.hpp"
#include "qrb_ros_manipulator_msgs/srv/manipulator_move_joint_pose.hpp"
#include "qrb_ros_manipulator_msgs/srv/manipulator_get_joint_pose.hpp"
#include "qrb_ros_manipulator_msgs/srv/manipulator_move_tcp_pose.hpp"
#include "qrb_ros_manipulator_msgs/srv/manipulator_get_tcp_pose.hpp"
#include "qrb_ros_manipulator_msgs/srv/manipulator_target_reachable.hpp"
#include "qrb_ros_manipulator_msgs/srv/manipulator_claw_control.hpp"
#include "qrb_ros_manipulator_msgs/srv/manipulator_claw_get_status.hpp"

#include "qrb_robot_arm_manager/arm_manager_interface.hpp"

namespace qrb_ros
{
namespace arm_service
{
class ManipulatorControlClient : public rclcpp::Node
{
public:
  using SrvManipulatorGetControlMode = qrb_ros_manipulator_msgs::srv::ManipulatorGetControlMode;
  using SrvManipulatorSetControlMode = qrb_ros_manipulator_msgs::srv::ManipulatorSetControlMode;
  using SrvManipulatorMoveJointPose = qrb_ros_manipulator_msgs::srv::ManipulatorMoveJointPose;
  using SrvManipulatorGetJointPose = qrb_ros_manipulator_msgs::srv::ManipulatorGetJointPose;
  using SrvManipulatorMoveTcpPose = qrb_ros_manipulator_msgs::srv::ManipulatorMoveTcpPose;
  using SrvManipulatorGetTcpPose = qrb_ros_manipulator_msgs::srv::ManipulatorGetTcpPose;
  using SrvManipulatorTargetReachable = qrb_ros_manipulator_msgs::srv::ManipulatorTargetReachable;
  using SrvManipulatorClawControl = qrb_ros_manipulator_msgs::srv::ManipulatorClawControl;
  using SrvManipulatorClawGetStatus = qrb_ros_manipulator_msgs::srv::ManipulatorClawGetStatus;

  explicit ManipulatorControlClient(const rclcpp::NodeOptions& options);

private:
  rclcpp::CallbackGroup::SharedPtr set_callback_group_{ nullptr };
  rclcpp::CallbackGroup::SharedPtr get_callback_group_{ nullptr };

  rclcpp::Client<SrvManipulatorGetControlMode>::SharedPtr get_control_mode_client_{ nullptr };
  rclcpp::Client<SrvManipulatorSetControlMode>::SharedPtr set_control_mode_client_{ nullptr };

  rclcpp::Client<SrvManipulatorMoveJointPose>::SharedPtr move_joint_pose_client_{ nullptr };
  rclcpp::Client<SrvManipulatorGetJointPose>::SharedPtr get_joint_pose_client_{ nullptr };

  rclcpp::Client<SrvManipulatorMoveTcpPose>::SharedPtr move_tcp_pose_client_{ nullptr };
  rclcpp::Client<SrvManipulatorGetTcpPose>::SharedPtr get_tcp_pose_client_{ nullptr };

  rclcpp::Client<SrvManipulatorTargetReachable>::SharedPtr target_reachable_client_{ nullptr };

  rclcpp::Client<SrvManipulatorClawControl>::SharedPtr claw_control_client_{ nullptr };
  rclcpp::Client<SrvManipulatorClawGetStatus>::SharedPtr get_claw_status_client_{ nullptr };

  const std::string get_control_mode_srv_name_ = "manipulator_get_control_mode";
  const std::string set_control_mode_srv_name_ = "manipulator_set_control_mode";
  const std::string move_joint_pose_srv_name_ = "manipulator_move_joint_pose";
  const std::string get_joint_pose_srv_name_ = "manipulator_get_joint_pose";
  const std::string move_tcp_pose_srv_name_ = "manipulator_move_tcp_pose";
  const std::string get_tcp_pose_srv_name_ = "manipulator_get_tcp_pose";
  const std::string target_reachable_srv_name_ = "manipulator_target_reachable";
  const std::string claw_control_srv_name_ = "manipulator_claw_control";
  const std::string claw_get_status_srv_name_ = "manipulator_claw_get_status";

  bool get_control_mode_func_(qrb::arm_manager::ManipulatorGetControlMode&,
                              std::chrono::milliseconds);
  bool set_control_mode_func_(qrb::arm_manager::ManipulatorSetControlMode&,
                              std::chrono::milliseconds);
  bool get_joint_pose_func_(qrb::arm_manager::ManipulatorGetJointPose&, std::chrono::milliseconds);
  bool move_joint_pose_func_(qrb::arm_manager::ManipulatorMoveJointPose&,
                             std::chrono::milliseconds);
  bool get_tcp_pose_func_(qrb::arm_manager::ManipulatorGetTcpPose&, std::chrono::milliseconds);
  bool move_tcp_pose_func_(qrb::arm_manager::ManipulatorMoveTcpPose&, std::chrono::milliseconds);
  bool check_target_reachable_func_(qrb::arm_manager::ManipulatorTargetReachable&,
                                    std::chrono::milliseconds);
  bool get_claw_status_func_(qrb::arm_manager::ManipulatorClawGetStatus&,
                             std::chrono::milliseconds);
  bool control_claw_func_(qrb::arm_manager::ManipulatorClawControl&, std::chrono::milliseconds);
};
}  // namespace arm_service
}  // namespace qrb_ros

#endif  // QRB_ROS_ARM_SERVICE__MANIPULATOR_CONTROL_CLIENT_HPP_