// Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#ifndef QRB_ROS_ARM_SERVICE__MOTION_SERVICE_CALLER_ACTION_CLIENT_HPP_
#define QRB_ROS_ARM_SERVICE__MOTION_SERVICE_CALLER_ACTION_CLIENT_HPP_

#include "rclcpp/rclcpp.hpp"
#include "rclcpp_action/rclcpp_action.hpp"

#include "qrb_ros_motion_msgs/action/line_motion.hpp"
#include "qrb_ros_motion_msgs/action/rotation_motion.hpp"

#include "qrb_robot_arm_manager/arm_manager_interface.hpp"

namespace qrb_ros
{
namespace arm_service
{
class MotionServiceCallerActionClient : public rclcpp::Node
{
public:
  using LineMotion = qrb_ros_motion_msgs::action::LineMotion;
  using GoalHandleLineMotion = rclcpp_action::ClientGoalHandle<LineMotion>;

  using RotationMotion = qrb_ros_motion_msgs::action::RotationMotion;
  using GoalHandleRotationMotion = rclcpp_action::ClientGoalHandle<RotationMotion>;

  explicit MotionServiceCallerActionClient(const rclcpp::NodeOptions& options);

private:
  rclcpp::node_interfaces::NodeBaseInterface::SharedPtr node_;
  rclcpp::CallbackGroup::SharedPtr callback_group_{ nullptr };

  rclcpp_action::Client<LineMotion>::SharedPtr line_motion_client_{ nullptr };
  rclcpp_action::Client<RotationMotion>::SharedPtr rotation_motion_client_{ nullptr };

  const std::string line_motion_name_ = "line_motion";
  const std::string rotation_motion_name_ = "rotation_motion";

  bool send_line_motion_goal_(LineMotion::Goal& goal);
  bool send_rotation_motion_goal_(RotationMotion::Goal& goal);
  bool send_motion_goal_(const qrb::arm_manager::AmrMotionMsg& data);
};
}  // namespace arm_service
}  // namespace qrb_ros
#endif  // QRB_ROS_ARM_SERVICE__MOTION_SERVICE_CALLER_ACTION_CLIENT_HPP_