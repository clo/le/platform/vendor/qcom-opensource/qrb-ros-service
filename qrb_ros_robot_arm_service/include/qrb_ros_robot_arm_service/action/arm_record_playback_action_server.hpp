// Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#ifndef QRB_ROS_ARM_SERVICE__ARM_RECORD_PLAYBACK_SERVER_HPP_
#define QRB_ROS_ARM_SERVICE__ARM_RECORD_PLAYBACK_SERVER_HPP_

#include "rclcpp/rclcpp.hpp"
#include "rclcpp_action/rclcpp_action.hpp"

#include "qrb_ros_robot_arm_service_msgs/action/arm_service_record.hpp"
#include "qrb_ros_robot_arm_service_msgs/action/arm_service_playback.hpp"

#include "qrb_robot_arm_manager/arm_manager_interface.hpp"

namespace qrb_ros
{
namespace arm_service
{
class ArmRecordPlaybackActionServer : public rclcpp::Node
{
public:
  using ArmServiceRecord = qrb_ros_robot_arm_service_msgs::action::ArmServiceRecord;
  using GoalHandleArmServiceRecord = rclcpp_action::ServerGoalHandle<ArmServiceRecord>;

  using ArmServicePlayback = qrb_ros_robot_arm_service_msgs::action::ArmServicePlayback;
  using GoalHandleArmServicePlayback = rclcpp_action::ServerGoalHandle<ArmServicePlayback>;

  explicit ArmRecordPlaybackActionServer(const rclcpp::NodeOptions& options);

private:
  rclcpp::CallbackGroup::SharedPtr callback_group_{ nullptr };
  rclcpp_action::Server<ArmServiceRecord>::SharedPtr arm_record_server_{ nullptr };
  rclcpp_action::Server<ArmServicePlayback>::SharedPtr arm_playback_server_{ nullptr };

  uint8_t record_cmd_{ 0 };

  std::string arm_record_action_name_ = "arm_record_action";
  std::string arm_playback_action_name_ = "arm_playback_action";

  std::shared_ptr<qrb::arm_manager::ArmRecordPlaybackBase> arm_record_playback_instance_{ nullptr };

  rclcpp_action::GoalResponse arm_record_handle_goal_(
      const rclcpp_action::GoalUUID& uuid, std::shared_ptr<const ArmServiceRecord::Goal> goal);
  rclcpp_action::CancelResponse
  arm_record_handle_cancel_(const std::shared_ptr<GoalHandleArmServiceRecord> goal_handle);
  void arm_record_handle_accepted_(const std::shared_ptr<GoalHandleArmServiceRecord> goal_handle);

  rclcpp_action::GoalResponse arm_playback_handle_goal_(
      const rclcpp_action::GoalUUID& uuid, std::shared_ptr<const ArmServicePlayback::Goal> goal);
  rclcpp_action::CancelResponse
  arm_playback_handle_cancel_(const std::shared_ptr<GoalHandleArmServicePlayback> goal_handle);
  void
  arm_playback_handle_accepted_(const std::shared_ptr<GoalHandleArmServicePlayback> goal_handle);

  bool arm_record_publish_feedback_(const std::shared_ptr<GoalHandleArmServiceRecord> goal_handle,
                                    qrb::arm_manager::ArmRecordPlaybackFeedback data);
  bool arm_record_publish_result_(const std::shared_ptr<GoalHandleArmServiceRecord> goal_handle,
                                  bool result);

  bool
  arm_playback_publish_feedback_(const std::shared_ptr<GoalHandleArmServicePlayback> goal_handle,
                                 qrb::arm_manager::ArmRecordPlaybackFeedback data);
  bool arm_playback_publish_result_(const std::shared_ptr<GoalHandleArmServicePlayback> goal_handle,
                                    bool result);
};
}  // namespace arm_service
}  // namespace qrb_ros
#endif  // QRB_ROS_ARM_SERVICE__ARM_RECORD_PLAYBACK_SERVER_HPP_