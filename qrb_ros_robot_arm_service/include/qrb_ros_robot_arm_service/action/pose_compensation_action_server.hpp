// Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear

#ifndef QRB_ROS_ARM_SERVICE__POSE_COMPENSATION_ACTION_SERVER_HPP_
#define QRB_ROS_ARM_SERVICE__POSE_COMPENSATION_ACTION_SERVER_HPP_

#include "rclcpp/rclcpp.hpp"
#include "rclcpp_action/rclcpp_action.hpp"

#include "qrb_ros_robot_arm_service_msgs/action/arm_service_pose_compensation.hpp"

#include "qrb_robot_arm_manager/arm_manager_interface.hpp"

namespace qrb_ros
{
namespace arm_service
{
class PoseCompensationActionServer : public rclcpp::Node
{
public:
  using PoseCompensationAction = qrb_ros_robot_arm_service_msgs::action::ArmServicePoseCompensation;
  using GoalHandlePoseCompensationAction = rclcpp_action::ServerGoalHandle<PoseCompensationAction>;

  explicit PoseCompensationActionServer(const rclcpp::NodeOptions& options);

private:
  rclcpp::CallbackGroup::SharedPtr callback_group_{ nullptr };
  rclcpp_action::Server<PoseCompensationAction>::SharedPtr pose_compensation_server_{ nullptr };

  const std::string pose_compensation_action_name_ = "pose_compensation_action";

  std::shared_ptr<qrb::arm_manager::PoseCompensationBase> pose_compensation_instance_{ nullptr };

  rclcpp_action::GoalResponse
  pose_compensation_handle_goal_(const rclcpp_action::GoalUUID& uuid,
                                 std::shared_ptr<const PoseCompensationAction::Goal> goal);
  rclcpp_action::CancelResponse pose_compensation_handle_cancel_(
      const std::shared_ptr<GoalHandlePoseCompensationAction> goal_handle);
  void pose_compensation_handle_accepted_(
      const std::shared_ptr<GoalHandlePoseCompensationAction> goal_handle);

  bool pose_compensation_publish_feedback_(
      const std::shared_ptr<GoalHandlePoseCompensationAction> goal_handle,
      qrb::arm_manager::PoseCompensationFeedback data);
  bool pose_compensation_publish_result_(
      const std::shared_ptr<GoalHandlePoseCompensationAction> goal_handle, bool result);
};
}  // namespace arm_service
}  // namespace qrb_ros
#endif  // QRB_ROS_ARM_SERVICE__POSE_COMPENSATION_ACTION_SERVER_HPP_