/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef VIRTUAL_PATH_SERVICE_SERVER_HPP_
#define VIRTUAL_PATH_SERVICE_SERVER_HPP_

#include "navigation_service/navigation_service.hpp"

using namespace qrb::navigation_manager;

namespace qrb_ros {
namespace navigation {

/**
 * @class navigation_controller::VirtualPathServiceServer
 * @desc The VirtualPathServiceServer create service server to receive requests
 * from amr_daemon.
 */
class VirtualPathServiceServer : public rclcpp::Node {
public:
  VirtualPathServiceServer(std::shared_ptr<NavigationService> &service);

  ~VirtualPathServiceServer();

private:
  void init_server();

  void
  receive_request(const std::shared_ptr<VirtualPathService::Request> request,
                  std::shared_ptr<VirtualPathService::Response> response);

  std::shared_ptr<rclcpp::Service<VirtualPathService>> service_;
  std::shared_ptr<NavigationService> navigation_service_;
  rclcpp::Logger logger_{rclcpp::get_logger("virtual_path_service_server")};
};

} // namespace navigation
} // namespace qrb_ros
#endif // VIRTUAL_PATH_SERVICE_SERVER_HPP_