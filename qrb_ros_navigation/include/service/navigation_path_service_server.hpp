/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef NAVIGATION_PATH_SERVICE_SERVER_HPP_
#define NAVIGATION_PATH_SERVICE_SERVER_HPP_

#include "navigation_service/navigation_service.hpp"
#include "qrb_ros_navigation_msgs/srv/compute_follow_path.hpp"
#include "qrb_ros_navigation_msgs/srv/compute_p2p_path.hpp"

using namespace qrb::navigation_manager;

using ComputeFollowPath = qrb_ros_navigation_msgs::srv::ComputeFollowPath;
using ComputeP2pPath = qrb_ros_navigation_msgs::srv::ComputeP2pPath;

namespace qrb_ros {
namespace navigation {

class NavigationPathServiceServer : public rclcpp::Node {
public:
  NavigationPathServiceServer(std::shared_ptr<NavigationService> &service);

  ~NavigationPathServiceServer();

private:
  void init_server();

  void receive_follow_path_request(
      const std::shared_ptr<ComputeFollowPath::Request> request,
      std::shared_ptr<ComputeFollowPath::Response> response);
  void
  receive_p2p_request(const std::shared_ptr<ComputeP2pPath::Request> request,
                      std::shared_ptr<ComputeP2pPath::Response> response);
  void print_follow_path(std::vector<uint32_t> &path);

  std::shared_ptr<rclcpp::Service<ComputeFollowPath>> server_;
  std::shared_ptr<rclcpp::Service<ComputeP2pPath>> server2_;

  std::shared_ptr<NavigationService> navigation_service_;

  rclcpp::Logger logger_{rclcpp::get_logger("navigation_path_service_server")};
};

} // namespace navigation
} // namespace qrb_ros
#endif // NAVIGATION_PATH_SERVICE_SERVER_HPP_