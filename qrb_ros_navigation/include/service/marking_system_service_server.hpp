/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef MARKING_SYSTEM_SERVICE_SERVER_HPP_
#define MARKING_SYSTEM_SERVICE_SERVER_HPP_

#include "geometry_msgs/msg/pose_stamped.hpp"
#include "navigation_service/common.hpp"
#include "navigation_service/navigation_service.hpp"
#include "qrb_ros_amr_msgs/srv/narrow_area.hpp"
#include "rclcpp/rclcpp.hpp"
#include "topic/tf_subscriber.hpp"

using namespace qrb::navigation_manager;

using NarrowArea = qrb_ros_amr_msgs::srv::NarrowArea;
using Point = geometry_msgs::msg::Point;
using Pose = geometry_msgs::msg::PoseStamped;

namespace qrb_ros {
namespace navigation {

/**
 * @class navigation_controller::MarkingSystemServiceServer
 * @desc The MarkingSystemServiceServer create service service to receive
 * requests from amr_daemon.
 */
class MarkingSystemServiceServer : public rclcpp::Node {
public:
  MarkingSystemServiceServer(std::shared_ptr<NavigationService> &service);

  ~MarkingSystemServiceServer();

private:
  void init_server();

  void receive_request(const std::shared_ptr<NarrowArea::Request> request,
                       std::shared_ptr<NarrowArea::Response> response);

  uint8_t
  parse_request_to_api(const std::shared_ptr<NarrowArea::Request> request);

  void parse_request_to_narrow_area(
      const std::shared_ptr<NarrowArea::Request> &request, narrow_area &area);

  void
  parse_request_to_2d_point(const std::shared_ptr<NarrowArea::Request> &request,
                            point_2d &point);

  uint8_t
  parse_request_to_area_id(const std::shared_ptr<NarrowArea::Request> request);

  void convert_pose_to_2d_point(Pose pose, point_2d &point);

  bool is_area_valid(narrow_area &area);

  bool is_point_valid(point_2d &point);

  std::shared_ptr<rclcpp::Service<NarrowArea>> service_;
  std::shared_ptr<NavigationService> navigation_service_;
  rclcpp::Logger logger_{rclcpp::get_logger("marking_system_service_server")};
};

} // namespace navigation
} // namespace qrb_ros
#endif // MARKING_SYSTEM_SERVICE_SERVER_HPP_
