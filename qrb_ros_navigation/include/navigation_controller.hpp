/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef NAVIGATION_CONTROLLER_HPP_
#define NAVIGATION_CONTROLLER_HPP_

#include "action/follow_path_action_server.hpp"
#include "action/navigation_action_server.hpp"
#include "action/qti_follow_path_action_server.hpp"
#include "service/marking_system_service_server.hpp"
#include "service/navigation_path_service_server.hpp"
#include "service/virtual_path_service_server.hpp"
#include "topic/exception_subscriber.hpp"
#include "topic/odom_subscriber.hpp"
#include "topic/path_publisher.hpp"
#include "topic/tf_subscriber.hpp"
#include "topic/twist_publisher.hpp"
#include <mutex>
#include <string.h>

namespace qrb_ros {
namespace navigation {
/**
 * @class navigation_controller::NavigationController
 * @desc The NavigationController create nodes to control the Navigation.
 */
class NavigationController : public rclcpp::Node {
public:
  std::shared_ptr<rclcpp::executors::MultiThreadedExecutor> executor_;

  /**
   * @desc A constructor for NavigationController
   */
  NavigationController();

  /**
   * @desc A destructor for NavigationController
   */
  ~NavigationController();

private:
  void init_nodes();

  std::mutex request_mutex_;
  std::string local_conf_path_ = "navigation_ros/config/local_costmap.yaml";
  std::string global_conf_path_ = "navigation_ros/config/global_costmap.yaml";

  std::shared_ptr<NavigationService> navigation_service_;
  std::shared_ptr<MarkingSystemServiceServer> ms_service_server_;
  std::shared_ptr<TFSubscriber> tf_subscriber_;
  std::shared_ptr<FollowPathActionServer> follow_path_server_;
  std::shared_ptr<QTIFollowPathActionServer> qti_follow_path_server_;
  std::shared_ptr<NavigationActionServer> navigation_server_;
  std::shared_ptr<ExceptionSubscriber> exception_subscriber_;
  std::shared_ptr<OdomSubscriber> odom_sub_;
  std::shared_ptr<TwistPublisher> vel_publisher_;
  std::shared_ptr<PathPublisher> path_publisher_;
  std::shared_ptr<VirtualPathServiceServer> virtual_path_service_server_;
  std::shared_ptr<NavigationPathServiceServer> navigation_path_service_server_;

  rclcpp::Logger logger_{rclcpp::get_logger("navigation_controller")};
};

} // namespace navigation
} // namespace qrb_ros
#endif // NAVIGATION_CONTROLLER_HPP_