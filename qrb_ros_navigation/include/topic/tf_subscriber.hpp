/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef TF_SUBSCRIBER_NODE_HPP_
#define TF_SUBSCRIBER_NODE_HPP_

#include <string>

#include "geometry_msgs/msg/pose_stamped.hpp"
#include "geometry_msgs/msg/transform_stamped.hpp"
#include "rclcpp/rclcpp.hpp"
#include "tf2/LinearMath/Quaternion.h"
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"
#include "tf2_ros/buffer.h"
#include "tf2_ros/transform_listener.h"

#include "action/follow_path_action_server.hpp"
#include "action/navigation_action_server.hpp"
#include "action/qti_follow_path_action_server.hpp"
#include "navigation_service/navigation_service.hpp"

using namespace qrb::navigation_manager;

namespace qrb_ros {
namespace navigation {

// TODO
// get the pose of radar in the robot, susggest use config file
// use TransformListener to get TF to convert the post of robot in map;
class TFSubscriber : public rclcpp::Node {
public:
  TFSubscriber(std::shared_ptr<NavigationService> &service,
               std::shared_ptr<FollowPathActionServer> &follow_path,
               std::shared_ptr<QTIFollowPathActionServer> &qti_follow_path,
               std::shared_ptr<NavigationActionServer> &navigation);

  ~TFSubscriber();

  PoseStamped get_current_pose();

private:
  void init_subscriber();

  void convert_tf_to_pose();

  void update_amr_pose(PoseStamped &pose);

  bool is_pose_change();

  bool is_equal(double a, double b);

  void pose_changed_callback(const PoseStamped::SharedPtr pose);
  void get_current_point2(point_2d &p);
  void convert_pose_to_2d_point(PoseStamped &pose, point_2d &point);

  std::shared_ptr<NavigationService> navigation_service_;
  std::shared_ptr<FollowPathActionServer> follow_path_server_;
  std::shared_ptr<QTIFollowPathActionServer> qti_follow_path_server_;
  std::shared_ptr<NavigationActionServer> navigation_server_;
  rclcpp::Subscription<PoseStamped>::SharedPtr pose_sub_;
  std::shared_ptr<tf2_ros::Buffer> tf_buffer_;
  std::shared_ptr<tf2_ros::TransformListener> tf_listener_;
  get_current_point_func_t get_current_point_cb_;

  std::string target_frame_ = "map";
  std::string source_frame_ = "base_link";
  PoseStamped source_pose_;
  PoseStamped target_pose_;
  PoseStamped last_pose_;
  rclcpp::TimerBase::SharedPtr timer_{nullptr};
  std::mutex mtx_;
  bool tf_working_;
  uint64_t count_;

  rclcpp::Logger logger_{rclcpp::get_logger("tf_subscriber")};
};

} // namespace navigation
} // namespace qrb_ros
#endif // TF_SUBSCRIBER_NODE_HPP_