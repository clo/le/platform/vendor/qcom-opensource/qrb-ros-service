/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef NAVIGATION_PATH_PUBLISHER
#define NAVIGATION_PATH_PUBLISHER

#include "geometry_msgs/msg/pose_stamped.hpp"
#include "nav_msgs/msg/path.hpp"
#include "navigation_service/common.hpp"
#include "navigation_service/navigation_service.hpp"
#include "rclcpp/rclcpp.hpp"
#include <string>
#include <vector>

using namespace qrb::navigation_manager;

namespace qrb_ros {
namespace navigation {

class PathPublisher : public rclcpp::Node {
public:
  PathPublisher(std::shared_ptr<NavigationService> &service);
  ~PathPublisher();

  void publish_global_path(const std::vector<point_2d> &point_list,
                           double &origin_x, double &origin_y);
  void publish_local_path(const std::vector<point_2d> &point_list,
                          double &origin_x, double &origin_y);
  void publish_local_shape_path(const std::vector<point_2d> &point_list,
                                double &origin_x, double &origin_y);
  void publish_backward_shape_path(const std::vector<point_2d>& point_list,
                                   double& origin_x, double& origin_y);
  void publish_narrow_info(std::vector<point_2d> &point_list, bool is_area);
  void publish_real_path(const std::vector<point_2d> &point_list);

private:
  rclcpp::Publisher<nav_msgs::msg::Path>::SharedPtr global_pub_;
  rclcpp::Publisher<nav_msgs::msg::Path>::SharedPtr local_pub_;
  rclcpp::Publisher<nav_msgs::msg::Path>::SharedPtr local_shape_pub_;
  rclcpp::Publisher<nav_msgs::msg::Path>::SharedPtr narrow_pub_;
  rclcpp::Publisher<nav_msgs::msg::Path>::SharedPtr area_pub_;
  rclcpp::Publisher<nav_msgs::msg::Path>::SharedPtr real_pub_;
  rclcpp::Publisher<nav_msgs::msg::Path>::SharedPtr backward_pub_;

  publish_real_path_func_t publish_real_path_cb_;
  publish_global_path_func_t publish_global_path_cb_;
  publish_local_path_func_t publish_local_path_cb_;
  publish_local_path_func_t publish_local_shape_path_cb_;
  publish_local_path_func_t publish_backward_shape_path_cb_;
  std::shared_ptr<NavigationService> navigation_service_;

  rclcpp::Logger logger_{rclcpp::get_logger("path_publisher")};
};

} // namespace navigation
} // namespace qrb_ros
#endif