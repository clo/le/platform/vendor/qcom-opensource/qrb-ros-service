/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef NAVIGATION_TWIST_PUBLISHER_HPP_
#define NAVIGATION_TWIST_PUBLISHER_HPP_

#include <memory>
#include <condition_variable>
#include <mutex>
#include <pthread.h>

#include "geometry_msgs/msg/twist.hpp"
#include "navigation_service/navigation_service.hpp"
#include "unistd.h"

using namespace qrb::navigation_manager;

namespace qrb_ros {
namespace navigation {

/**
 * @class navigator::TwistPublisher
 * @desc Publish twist data to the RobotController
 * data
 */
class TwistPublisher : public rclcpp::Node {
public:
  /**
   * @desc A constructor for navigator::TwistPublisher
   */
  TwistPublisher(std::shared_ptr<NavigationService> &service);

  /**
   * @desc A destructor for navigator::TwistPublisher class
   */
  ~TwistPublisher();

  /**
   * @desc Send the velocity data to robot
   * @param twist velocity in free space broken into its linear and angular
   * parts
   */
  void send_velocity(geometry_msgs::msg::Twist &velocity);

private:
  /**
   * @desc Initialize publisher
   */
  void init_publisher();

  rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr pub_;
  publish_twist_func_t publish_twist_cb_;
  publish_uninterruptable_twist_func_t publish_uninterruptable_twist_cb_;
  std::shared_ptr<NavigationService> navigation_service_;
  std::mutex vel_mtx_;
  rclcpp::Logger logger_{rclcpp::get_logger("twist_publisher")};
};

} // namespace navigation
} // namespace qrb_ros
#endif // NAVIGATION_TWIST_PUBLISHER_HPP_
