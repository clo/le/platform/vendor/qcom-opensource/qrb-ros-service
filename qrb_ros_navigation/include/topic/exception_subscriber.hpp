/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef NAVIGATION_EXCEPTION_SUBSCRIBER_NODE_HPP_
#define NAVIGATION_EXCEPTION_SUBSCRIBER_NODE_HPP_

#include "navigation_service/navigation_service.hpp"
#include <sensor_msgs/msg/laser_scan.hpp>
#include "std_msgs/msg/int16.hpp"

using namespace qrb::navigation_manager;

namespace qrb_ros {
namespace navigation {
enum class EmergencyType {
  ENTER = 0,
  EXIT = 1,
};

class ExceptionSubscriber : public rclcpp::Node {
public:
  ExceptionSubscriber(std::shared_ptr<NavigationService> &service);

  ~ExceptionSubscriber();

private:
  void init_subscriber();

  void topic_callback(const Exception::SharedPtr msg);
  void laser_callback(sensor_msgs::msg::LaserScan::ConstSharedPtr scan);
  void developer_mode_callback(const std_msgs::msg::Int16::ConstSharedPtr msg);

  rclcpp::Subscription<Exception>::SharedPtr subscriber_;
  rclcpp::Subscription<sensor_msgs::msg::LaserScan>::SharedPtr laser_sub_;
  rclcpp::Subscription<std_msgs::msg::Int16>::SharedPtr sub_;
  std::shared_ptr<NavigationService> navigation_service_;
  bool emergency_braking_;

  rclcpp::Logger logger_{rclcpp::get_logger("exception_subscriber")};
};

} // namespace navigation
} // namespace qrb_ros
#endif // NAVIGATION_EXCEPTION_SUBSCRIBER_NODE_HPP_