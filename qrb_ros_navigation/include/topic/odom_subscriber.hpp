/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef NAVIGATION_ODOM_SUBSCRIBER_HPP_
#define NAVIGATION_ODOM_SUBSCRIBER_HPP_

#include <memory>
#include <mutex>

#include "nav_2d_msgs/msg/twist2_d.hpp"
#include "nav_2d_msgs/msg/twist2_d_stamped.hpp"
#include "nav_msgs/msg/odometry.hpp"
#include "navigation_service/navigation_service.hpp"
#include "rclcpp/rclcpp.hpp"

using namespace qrb::navigation_manager;

namespace qrb_ros {
namespace navigation {

/**
 * @class navigator::OdomSubscriber
 * @desc Subscribe the odom data to get the velocity of robot
 * data
 */
class OdomSubscriber : public rclcpp::Node {
public:
  /**
   * @desc A constructor for navigator::OdomSubscriber
   */
  explicit OdomSubscriber(std::shared_ptr<NavigationService> &service);

  /**
   * @desc A destructor for navigator::OdomSubscriber class
   */
  ~OdomSubscriber();

  /**
   * @desc Get the current velocity of robot
   * @param twist velocity in free space broken into its linear and angular
   * parts
   */
  void get_robot_velocity(nav_2d_msgs::msg::Twist2D &twist);

private:
  /**
   * @desc Initialize subscriber
   */
  void init_subscriber();

  void odom_callback(const nav_msgs::msg::Odometry::SharedPtr msg);

  rclcpp::Subscription<nav_msgs::msg::Odometry>::SharedPtr sub_;
  nav_2d_msgs::msg::Twist2DStamped odom_velocity_;
  std::mutex mutex_;
  get_robot_velocity_func_t get_robot_velocity_cb_;
  std::shared_ptr<NavigationService> navigation_service_;

  rclcpp::Logger logger_{rclcpp::get_logger("odom_subscriber")};
};

} // namespace navigation
} // namespace qrb_ros
#endif // NAVIGATION_ODOM_SUBSCRIBER_HPP_
