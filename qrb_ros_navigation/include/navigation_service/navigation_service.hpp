/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef NAVIGATION_SERVICE_HPP_
#define NAVIGATION_SERVICE_HPP_

#include "geometry_msgs/msg/pose_stamped.hpp"
#include "geometry_msgs/msg/twist.hpp"
#include "nav2_msgs/action/follow_path.hpp"
#include "nav2_msgs/action/navigate_to_pose.hpp"
#include "nav_2d_msgs/msg/twist2_d.hpp"
#include "nav_msgs/msg/path.hpp"
#include "navigation_service/common.hpp"
#include "qrb_ros_navigation_msgs/action/follow_path.hpp"
#include "qrb_ros_navigation_msgs/action/navigate_to_position.hpp"
#include "qrb_ros_navigation_msgs/srv/virtual_path.hpp"
#include "qrb_ros_robot_base_msgs/msg/exception.hpp"
#include "rclcpp/rclcpp.hpp"
#include "rclcpp_action/rclcpp_action.hpp"

#include "qrb_navigation_manager/mannual_marker.hpp"
#include "qrb_navigation_manager/follow_path_planner.hpp"
#include "qrb_navigation_manager/global_path_planner.hpp"
#include "qrb_navigation_manager/local_path_planner.hpp"
#include "qrb_navigation_manager/nav_planner.hpp"
#include "qrb_navigation_manager/pid_controller.hpp"

using Path = nav_msgs::msg::Path;
using PoseStamped = geometry_msgs::msg::PoseStamped;
using NavigateToPose = nav2_msgs::action::NavigateToPose;
using GoalHandleNavigate = rclcpp_action::ServerGoalHandle<NavigateToPose>;
using FollowPath = nav2_msgs::action::FollowPath;
using GoalHandleFollow = rclcpp_action::ServerGoalHandle<FollowPath>;
using VirtualPathService = qrb_ros_navigation_msgs::srv::VirtualPath;
using Exception = qrb_ros_robot_base_msgs::msg::Exception;
using NavigateToPosition = qrb_ros_navigation_msgs::action::NavigateToPosition;
using GoalHandleNavigateToPosition = rclcpp_action::ServerGoalHandle<NavigateToPosition>;

namespace qrb {
namespace navigation_manager {
typedef std::function<void(uint64_t request_id, bool result)>
    navigation_completed_func_t;
typedef std::function<void(std::vector<point_2d> &path)>
    publish_real_path_func_t;
typedef std::function<void(std::vector<point_2d> &path, double x, double y)>
    publish_global_path_func_t;
typedef std::function<void(std::vector<point_2d> &path, double x, double y)>
    publish_local_path_func_t;
typedef std::function<void(geometry_msgs::msg::Twist &velocity)>
    publish_twist_func_t;
typedef std::function<void(geometry_msgs::msg::Twist& velocity, uint32_t duration)>
    publish_uninterruptable_twist_func_t;
typedef std::function<void(nav_2d_msgs::msg::Twist2D &twist)>
    get_robot_velocity_func_t;
typedef std::function<void(point_2d &p)> get_current_point_func_t;


/**
 * @class navigation_controller::NavigationService
 * @desc The NavigationService create nodes to control the Navigation.
 */
class NavigationService {
public:
  /**
   * @desc A constructor for NavigationService
   */
  NavigationService(std::string &local_conf_path,
                    std::string &global_conf_path);

  /**
   * @desc A destructor for NavigationService
   */
  ~NavigationService();

  // VirtualPathServiceServer
  uint32_t add_waypoint(point_2d &point);
  bool remove_waypoint(uint32_t id);
  bool get_waypoint_id_list(std::vector<uint32_t> &list);
  bool get_waypoint(uint32_t id, point_2d &point);
  bool add_virtual_path(uint32_t id, std::vector<uint32_t> &ids);
  bool remove_virtual_path(uint32_t id, std::vector<uint32_t> &ids);
  bool get_virtual_path(uint32_t id, std::vector<uint32_t> &ids);
  uint32_t add_target_waypoint(point_2d &point);
  void set_bypassing_obstacle(bool val);
  bool remove_waypoint_and_virtual_path();

  // FollowPathActionServer
  uint64_t request_follow_path(Path path);
  uint64_t request_follow_path(uint32_t goal, std::vector<uint32_t> &passing_ids);
  uint32_t get_passing_waypoint_id(point_2d &current_point);
  bool request_stop_follow_path_navigation();
  void register_navigation_callback(navigation_completed_func_t cb);
  bool update_current_pose(PoseStamped &pose);
  void request_pause_follow_path();
  void request_resume_follow_path();

  // NavigationActionServer
  uint64_t request_navigation(geometry_msgs::msg::PoseStamped &pose1,
                              geometry_msgs::msg::PoseStamped &pose2,
                              uint8_t type);
  uint64_t request_qrb_navigation(geometry_msgs::msg::PoseStamped &pose1,
                              point_2d &goal,
                              uint8_t type);

  void
  add_node(std::shared_ptr<rclcpp::executors::MultiThreadedExecutor> &executor);
  bool request_stop_p2p_navigation();

  // PathPublisher
  void register_publish_real_path_callback(publish_real_path_func_t cb);
  void register_publish_global_path_callback(publish_global_path_func_t cb);
  void register_publish_local_path_callback(publish_local_path_func_t cb);
  void register_publish_local_shape_path_callback(publish_local_path_func_t cb);
  void register_publish_backward_shape_path_callback(publish_local_path_func_t cb);

  // TwistPublisher
  void register_publish_twist_callback(publish_twist_func_t cb);
  void register_publish_uninterruptable_twist_callback(publish_uninterruptable_twist_func_t cb);
  void register_get_current_point_callback(get_current_point_func_t cb);

  // OdomSubscriber
  void register_get_robot_velocity_callback(get_robot_velocity_func_t cb);

  // ExceptionSubscriber
  void handle_amr_exception(bool exception);
  void handle_emergency(bool enter);
  void set_developer_mode(bool val);

  // NavigationPathServiceServer
  bool compute_follow_path(bool use_start, uint32_t start, uint32_t goal,
                           std::vector<uint32_t> &list,
                           std::vector<point_2d> &point_list);
  bool compute_follow_path(bool use_start, uint32_t start, uint32_t goal,
                           std::vector<uint32_t> &passing_ids,
                           std::vector<uint32_t> &list,
                           std::vector<point_2d> &point_list);
  bool compute_p2p_path(bool use_start, geometry_msgs::msg::PoseStamped &start,
                        geometry_msgs::msg::PoseStamped &goal,
                        nav_msgs::msg::Path &path);

private:
  void init();

  std::string local_conf_path_ = "navigator/config/local_costmap.yaml";
  std::string global_conf_path_ = "navigator/config/global_costmap.yaml";

  std::shared_ptr<MannualMarker> mannual_marker_;
  std::shared_ptr<FollowPathPlanner> follow_path_planner_;
  std::shared_ptr<GlobalPathPlanner> global_path_planner_;
  std::shared_ptr<NarrowPathPlanner> narrow_path_planner_;
  std::shared_ptr<LocalPathPlanner> local_path_planner_;
  std::shared_ptr<NavPlanner> algorithm_;
  std::shared_ptr<CostMap2DROS> local_cost_map_generator_;
  std::shared_ptr<CostMap2DROS> global_cost_map_generator_;
  std::shared_ptr<CostMap2DROS> global_static_cost_map_generator_;
  std::shared_ptr<PidController> robot_ctrl_;
  std::shared_ptr<VirtualPathManager> manager_;
  std::shared_ptr<ReducedTwistServiceClient> twist_client_;

  rclcpp::Logger logger_{rclcpp::get_logger("navigation_service")};
};
} // namespace navigation_manager
} // namespace qrb
#endif // NAVIGATION_SERVICE_HPP_