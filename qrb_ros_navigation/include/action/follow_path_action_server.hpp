/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef FOLLOW_PATH_ACTION_SERVER_NODE_HPP_
#define FOLLOW_PATH_ACTION_SERVER_NODE_HPP_

#include "navigation_service/navigation_service.hpp"
#include "qrb_ros_amr_msgs/srv/sub_cmd.hpp"

using namespace qrb::navigation_manager;

using FollowPathSubCmd = qrb_ros_amr_msgs::srv::SubCmd;

namespace qrb_ros {
namespace navigation {

class FollowPathActionServer : public rclcpp::Node {
public:
  FollowPathActionServer(std::shared_ptr<NavigationService> &service);

  ~FollowPathActionServer();

  void update_current_pose(PoseStamped &pose);

private:
  void init_server();

  rclcpp_action::GoalResponse
  handle_goal(const rclcpp_action::GoalUUID &uuid,
              std::shared_ptr<const FollowPath::Goal> goal);

  rclcpp_action::CancelResponse
  handle_cancel(const std::shared_ptr<GoalHandleFollow> goal_handle);

  void handle_accepted(const std::shared_ptr<GoalHandleFollow> goal_handle);

  bool is_pose_changed(PoseStamped &pose);

  void receive_navigation_callback(uint64_t request_id, bool result);

  void receive_request(const std::shared_ptr<FollowPathSubCmd::Request> request,
                       std::shared_ptr<FollowPathSubCmd::Response> response);

  std::shared_ptr<NavigationService> navigation_service_;
  rclcpp_action::Server<FollowPath>::SharedPtr action_server_ptr_;
  std::shared_ptr<GoalHandleFollow> server_global_handle_;
  rclcpp::Service<FollowPathSubCmd>::SharedPtr service_;

  PoseStamped current_pose_;
  qrb::navigation_manager::navigation_completed_func_t navigation_callback_;
  std::map<rclcpp_action::GoalUUID, uint64_t> request_id_map_;
  std::map<uint64_t, std::shared_ptr<GoalHandleFollow>> handle_map_;
  bool navigating_;

  rclcpp::Logger logger_{rclcpp::get_logger("foolow_path_action_server")};
};

} // namespace navigation
} // namespace qrb_ros
#endif // FOLLOW_PATH_ACTION_SERVER_NODE_HPP_