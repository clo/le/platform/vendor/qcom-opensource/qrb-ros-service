/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef QTI_FOLLOW_PATH_ACTION_SERVER_NODE_HPP_
#define QTI_FOLLOW_PATH_ACTION_SERVER_NODE_HPP_

#include "navigation_service/navigation_service.hpp"
#include "qrb_ros_navigation_msgs/action/follow_path.hpp"
#include "tf2/LinearMath/Quaternion.h"
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"
#include "tf2_ros/buffer.h"
#include "tf2_ros/transform_listener.h"

using namespace qrb::navigation_manager;

using QTIFollowPath = qrb_ros_navigation_msgs::action::FollowPath;
using GoalHandleQTIFollowPath = rclcpp_action::ServerGoalHandle<QTIFollowPath>;

namespace qrb_ros {
namespace navigation {

class QTIFollowPathActionServer : public rclcpp::Node {
public:
  QTIFollowPathActionServer(std::shared_ptr<NavigationService> &service);

  ~QTIFollowPathActionServer();

  void update_current_pose(PoseStamped &pose);

private:
  void init_server();

  rclcpp_action::GoalResponse
  handle_goal(const rclcpp_action::GoalUUID &uuid,
              std::shared_ptr<const QTIFollowPath::Goal> goal);

  rclcpp_action::CancelResponse
  handle_cancel(const std::shared_ptr<GoalHandleQTIFollowPath> goal_handle);

  void
  handle_accepted(const std::shared_ptr<GoalHandleQTIFollowPath> goal_handle);

  bool is_pose_changed(PoseStamped &pose);

  void receive_navigation_callback(uint64_t request_id, bool result);

  uint32_t get_passing_waypoint(PoseStamped &pose);
  void convert_pose_to_2d_point(PoseStamped &pose, point_2d &point);
  uint32_t get_passing_waypoint_id(point_2d &current_point);

  std::shared_ptr<NavigationService> navigation_service_;
  rclcpp_action::Server<QTIFollowPath>::SharedPtr action_server_ptr_;
  std::shared_ptr<GoalHandleQTIFollowPath> server_global_handle_;
  PoseStamped current_pose_;
  qrb::navigation_manager::navigation_completed_func_t navigation_callback_;
  std::map<rclcpp_action::GoalUUID, uint64_t> request_id_map_;
  std::map<uint64_t, std::shared_ptr<GoalHandleQTIFollowPath>> handle_map_;
  std::vector<uint32_t> passing_ids_;
  uint32_t next_passing_waypoing_index_;
  uint32_t current_passing_waypoint_id_;
  double current_distance_;
  double last_distance_2_;
  double last_distance_3_;
  bool navigating_;

  rclcpp::Logger logger_{rclcpp::get_logger("qti_follow_path_action_server")};
};

} // namespace navigation
} // namespace qrb_ros
#endif // QTI_FOLLOW_PATH_ACTION_SERVER_NODE_HPP_