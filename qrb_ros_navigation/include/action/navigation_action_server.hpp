/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#ifndef NAVIGATION_ACTION_SERVER_NODE_HPP_
#define NAVIGATION_ACTION_SERVER_NODE_HPP_

#include "navigation_service/navigation_service.hpp"
#include <mutex>

using namespace qrb::navigation_manager;

namespace qrb_ros {
namespace navigation {

class NavigationActionServer : public rclcpp::Node {
public:
  NavigationActionServer(std::shared_ptr<NavigationService> &service);

  ~NavigationActionServer();

  void update_current_pose(PoseStamped pose);

private:
  void init_server();

  rclcpp_action::GoalResponse
  handle_goal(const rclcpp_action::GoalUUID &uuid,
              std::shared_ptr<const NavigateToPose::Goal> goal);

  rclcpp_action::CancelResponse
  handle_cancel(const std::shared_ptr<GoalHandleNavigate> goal_handle);

  void handle_accepted(const std::shared_ptr<GoalHandleNavigate> goal_handle);

  void parse_goal_to_pose();

  bool is_pose_changed(PoseStamped pose);

  void receive_navigation_callback(uint64_t request_id, bool result);

  void receive_qrb_navigation_callback(uint64_t request_id, bool result);

  void receive_rviz_goal(const PoseStamped::SharedPtr pose);

  rclcpp_action::GoalResponse
  handle_qrb_goal(const rclcpp_action::GoalUUID &uuid,
              std::shared_ptr<const NavigateToPosition::Goal> goal);

  rclcpp_action::CancelResponse
  handle_qrb_cancel(const std::shared_ptr<GoalHandleNavigateToPosition> goal_handle);

  void handle_qrb_accepted(const std::shared_ptr<GoalHandleNavigateToPosition> goal_handle);

  rclcpp_action::Server<NavigateToPose>::SharedPtr action_server_ptr_;
  std::shared_ptr<GoalHandleNavigate> server_global_handle_;
  rclcpp_action::Server<NavigateToPosition>::SharedPtr qrb_action_server_ptr_;
  std::shared_ptr<GoalHandleNavigateToPosition> qrb_server_global_handle_;
  std::shared_ptr<NavigationService> navigation_service_;
  PoseStamped current_pose_;
  bool navigating_;

  qrb::navigation_manager::navigation_completed_func_t navigation_callback_;
  qrb::navigation_manager::navigation_completed_func_t qrb_navigation_callback_;

  std::map<rclcpp_action::GoalUUID, uint64_t> request_id_map_;
  std::map<uint64_t, std::shared_ptr<GoalHandleNavigate>> handle_map_;
  std::map<uint64_t, std::shared_ptr<GoalHandleNavigateToPosition>> qrb_handle_map_;

  rclcpp::Subscription<PoseStamped>::SharedPtr rviz_subscriber_;

  rclcpp::Logger logger_{rclcpp::get_logger("navigation_action_server")};
};

} // namespace navigation
} // namespace qrb_ros
#endif // NAVIGATION_ACTION_SERVER_NODE_HPP_