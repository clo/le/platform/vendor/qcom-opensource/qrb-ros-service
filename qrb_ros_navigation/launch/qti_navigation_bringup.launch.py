# Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear

from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node

from launch_ros.substitutions import FindPackageShare
from launch.substitutions import PathJoinSubstitution


def generate_launch_description():
    """Launch file to bring up qti navigation node standalone."""
    global_costmap_conf_file = PathJoinSubstitution(
        [FindPackageShare("qrb_ros_navigation"), "config", "global_costmap.yaml"]
    )
    local_costmap_conf_file = PathJoinSubstitution(
        [FindPackageShare("qrb_ros_navigation"), "config", "local_costmap.yaml"]
    )
    qti_navigation_node = Node(
        package='qrb_ros_navigation',
        executable='qrb_ros_navigation',
        parameters=[{
                    'local_costmap_conf': local_costmap_conf_file,
                    'global_costmap_conf': global_costmap_conf_file
                    }]
    )

    return LaunchDescription([qti_navigation_node])
