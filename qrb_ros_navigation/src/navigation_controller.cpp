/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include <pthread.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <utility>

#include "navigation_controller.hpp"

namespace qrb_ros {
namespace navigation {

NavigationController::NavigationController()
    : Node("NavigationController"),
      local_conf_path_(declare_parameter<std::string>(
          "local_costmap_conf",
          "qrb_ros_navigation/config/local_costmap.yaml")),
      global_conf_path_(declare_parameter<std::string>(
          "global_costmap_conf",
          "qrb_ros_navigation/config/global_costmap.yaml")) {
  std::unique_lock<std::mutex> lk(request_mutex_);
  init_nodes();
}

NavigationController::~NavigationController() {
  RCLCPP_INFO(logger_, "Destroy NavigationController");
}

void NavigationController::init_nodes() {
  RCLCPP_INFO(logger_, "init_nodes, local_conf_path_=%s, global_conf_path_=%s",
              local_conf_path_.c_str(), global_conf_path_.c_str());

  navigation_service_ = std::shared_ptr<NavigationService>(
      new NavigationService(local_conf_path_, global_conf_path_));

  executor_ = std::shared_ptr<rclcpp::executors::MultiThreadedExecutor>(
      new rclcpp::executors::MultiThreadedExecutor());

  virtual_path_service_server_ = std::shared_ptr<VirtualPathServiceServer>(
      new VirtualPathServiceServer(navigation_service_));

  odom_sub_ =
      std::shared_ptr<OdomSubscriber>(new OdomSubscriber(navigation_service_));

  vel_publisher_ =
      std::shared_ptr<TwistPublisher>(new TwistPublisher(navigation_service_));

  path_publisher_ =
      std::shared_ptr<PathPublisher>(new PathPublisher(navigation_service_));

  follow_path_server_ = std::shared_ptr<FollowPathActionServer>(
      new FollowPathActionServer(navigation_service_));

  qti_follow_path_server_ = std::shared_ptr<QTIFollowPathActionServer>(
      new QTIFollowPathActionServer(navigation_service_));

  navigation_server_ = std::shared_ptr<NavigationActionServer>(
      new NavigationActionServer(navigation_service_));

  tf_subscriber_ = std::shared_ptr<TFSubscriber>(
      new TFSubscriber(navigation_service_, follow_path_server_,
                       qti_follow_path_server_, navigation_server_));

  ms_service_server_ = std::shared_ptr<MarkingSystemServiceServer>(
      new MarkingSystemServiceServer(navigation_service_));

  exception_subscriber_ = std::shared_ptr<ExceptionSubscriber>(
      new ExceptionSubscriber(navigation_service_));

  navigation_path_service_server_ =
      std::shared_ptr<NavigationPathServiceServer>(
          new NavigationPathServiceServer(navigation_service_));

  executor_->add_node(path_publisher_);
  executor_->add_node(ms_service_server_);
  executor_->add_node(tf_subscriber_);
  executor_->add_node(follow_path_server_);
  executor_->add_node(qti_follow_path_server_);
  executor_->add_node(navigation_server_);
  executor_->add_node(exception_subscriber_);
  executor_->add_node(odom_sub_);
  executor_->add_node(vel_publisher_);
  executor_->add_node(virtual_path_service_server_);
  executor_->add_node(navigation_path_service_server_);
  navigation_service_->add_node(executor_);
}
} // namespace navigation
} // namespace qrb_ros
