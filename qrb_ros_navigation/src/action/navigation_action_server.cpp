/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include "action/navigation_action_server.hpp"

constexpr char const *node_name = "navigation_action_server";
constexpr char const *action_name = "p2p";
constexpr char const *qrb_action_name = "qrb_p2p";

namespace qrb_ros {
namespace navigation {
NavigationActionServer::NavigationActionServer(
    std::shared_ptr<NavigationService> &service)
    : Node(node_name), navigation_service_(service) {
  RCLCPP_INFO(logger_, "Creating");
  init_server();

  navigation_callback_ = [&](uint64_t request_id, bool result) {
    receive_navigation_callback(request_id, result);
  };
  navigation_service_->register_navigation_callback(navigation_callback_);

  qrb_navigation_callback_ = [&](uint64_t request_id, bool result) {
    receive_qrb_navigation_callback(request_id, result);
  };
  navigation_service_->register_navigation_callback(qrb_navigation_callback_);
}

NavigationActionServer::~NavigationActionServer() {
  RCLCPP_INFO(logger_, "Destroying");
}

void NavigationActionServer::init_server() {
  using namespace std::placeholders;

  action_server_ptr_ = rclcpp_action::create_server<NavigateToPose>(
      this, action_name,
      std::bind(&NavigationActionServer::handle_goal, this, _1, _2),
      std::bind(&NavigationActionServer::handle_cancel, this, _1),
      std::bind(&NavigationActionServer::handle_accepted, this, _1));

  qrb_action_server_ptr_ = rclcpp_action::create_server<NavigateToPosition>(
      this, qrb_action_name,
      std::bind(&NavigationActionServer::handle_qrb_goal, this, _1, _2),
      std::bind(&NavigationActionServer::handle_qrb_cancel, this, _1),
      std::bind(&NavigationActionServer::handle_qrb_accepted, this, _1));

  rviz_subscriber_ = this->create_subscription<PoseStamped>(
      "goal_pose", 10,
      std::bind(&NavigationActionServer::receive_rviz_goal, this, _1));
}

rclcpp_action::GoalResponse NavigationActionServer::handle_goal(
    const rclcpp_action::GoalUUID &uuid,
    std::shared_ptr<const NavigateToPose::Goal> goal) {
  PoseStamped goal_pose = goal->pose;
  uint8_t type = (uint8_t)Navigation_Type::P2P;
  char nav_type = goal->behavior_tree.c_str()[0];
  RCLCPP_INFO(logger_, "nav_type: %d", nav_type);
  if ('A' == nav_type) {
    type = (uint8_t)Navigation_Type::AE;
    RCLCPP_INFO(logger_, "Start P2P navigation from auto mapping2");
  } else {
    RCLCPP_INFO(logger_, "Start P2P navigation from amr controller");
  }

  if (navigation_service_ != nullptr) {
    uint64_t request_id =
        navigation_service_->request_navigation(current_pose_, goal_pose, type);
    if (request_id != 0) {
      RCLCPP_INFO(logger_, "request_navigation, request_id: %ld", request_id);
      navigating_ = true;
      request_id_map_.insert(
          std::pair<rclcpp_action::GoalUUID, uint64_t>(uuid, request_id));
      return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
    } else {
      RCLCPP_INFO(logger_, "request_navigation failed");
    }
  }
  return rclcpp_action::GoalResponse::REJECT;
}

rclcpp_action::CancelResponse NavigationActionServer::handle_cancel(
    const std::shared_ptr<GoalHandleNavigate> goal_handle) {
  RCLCPP_INFO(logger_, "Received request to cancel goal");

  if (navigation_service_ != nullptr) {
    if (navigation_service_->request_stop_p2p_navigation()) {
      return rclcpp_action::CancelResponse::ACCEPT;
    }
  }
  return rclcpp_action::CancelResponse::REJECT;
}

void NavigationActionServer::handle_accepted(
    const std::shared_ptr<GoalHandleNavigate> goal_handle) {
  rclcpp_action::GoalUUID goal_id = goal_handle->get_goal_id();
  uint64_t request_id = request_id_map_[goal_id];
  RCLCPP_INFO(logger_, "handle accepted,request_id:%ld", request_id);
  handle_map_.insert(std::pair<uint64_t, std::shared_ptr<GoalHandleNavigate>>(
      request_id, goal_handle));
}

void NavigationActionServer::parse_goal_to_pose() {}

void NavigationActionServer::update_current_pose(PoseStamped pose) {
  if (!navigating_) {
    RCLCPP_DEBUG(logger_, "current is not navigating");
    return;
  }

  if (!is_pose_changed(pose)) {
    RCLCPP_DEBUG(logger_, "pose is not change");
    return;
  }

  // feedback
  auto feedback = std::make_shared<NavigateToPose::Feedback>();
  feedback->current_pose = pose;

  std::shared_ptr<GoalHandleNavigate> handle;

  for (auto it : handle_map_) {
    handle = it.second;
    if (handle != nullptr && !(handle->is_canceling())) {
      handle->publish_feedback(feedback);
      RCLCPP_DEBUG(logger_, "feedback x:%f, y:%f", pose.pose.position.x,
                   pose.pose.position.y);
    }
  }
}

bool NavigationActionServer::is_pose_changed(PoseStamped pose) {
  bool is_changed = true;

  if (current_pose_.pose.position.x == pose.pose.position.x &&
      current_pose_.pose.position.y == pose.pose.position.y &&
      current_pose_.pose.position.z == pose.pose.position.z &&
      current_pose_.pose.orientation.x == pose.pose.orientation.x &&
      current_pose_.pose.orientation.y == pose.pose.orientation.y &&
      current_pose_.pose.orientation.z == pose.pose.orientation.z &&
      current_pose_.pose.orientation.w == pose.pose.orientation.w) {
    is_changed = false;
  }

  current_pose_.pose.position.x = pose.pose.position.x;
  current_pose_.pose.position.y = pose.pose.position.y;
  current_pose_.pose.position.z = pose.pose.position.z;
  current_pose_.pose.orientation.x = pose.pose.orientation.x;
  current_pose_.pose.orientation.y = pose.pose.orientation.y;
  current_pose_.pose.orientation.z = pose.pose.orientation.z;
  current_pose_.pose.orientation.w = pose.pose.orientation.w;
  current_pose_.header.stamp = pose.header.stamp;
  current_pose_.header.frame_id = pose.header.frame_id;
  return is_changed;
}

void NavigationActionServer::receive_navigation_callback(uint64_t request_id,
                                                         bool result) {
  RCLCPP_INFO(logger_, "receive_navigation_callback request_id:%ld, result:%d",
              request_id, result);
  if (!rclcpp::ok()) {
    return;
  }

  std::shared_ptr<GoalHandleNavigate> handle = handle_map_[request_id];

  auto action_result = std::make_shared<NavigateToPose::Result>();
  if (handle == nullptr) {
    RCLCPP_INFO(logger_, "get server global handle again");
    handle = server_global_handle_;
    if (handle == nullptr) {
      RCLCPP_INFO(logger_, "server global handle is nullptr");
      return;
    }
  }

  if (result) {
    handle->succeed(action_result);
    RCLCPP_INFO(logger_, "Goal succeeded");
  } else {
    if (handle->is_canceling()) {
      handle->canceled(action_result);
      RCLCPP_INFO(logger_, "Goal canceled");
    } else {
      handle->abort(action_result);
      RCLCPP_INFO(logger_, "Goal abort");
    }
  }

  request_id_map_.erase(handle->get_goal_id());
  handle_map_.erase(request_id);
  navigating_ = false;

  handle = nullptr;
  server_global_handle_ = nullptr;
}

void NavigationActionServer::receive_rviz_goal(
    const PoseStamped::SharedPtr pose) {
  RCLCPP_INFO(logger_, "Start P2P navigation from rviz");

  RCLCPP_INFO(logger_, "goal pose:(%f, %f, %f), orientation:(%f, %f, %f, %f)",
              pose->pose.position.x, pose->pose.position.y,
              pose->pose.position.z, pose->pose.orientation.x,
              pose->pose.orientation.y, pose->pose.orientation.z,
              pose->pose.orientation.w);

  PoseStamped goal_pose;
  goal_pose.pose.position.x = pose->pose.position.x;
  goal_pose.pose.position.y = pose->pose.position.y;
  goal_pose.pose.position.z = pose->pose.position.z;
  goal_pose.pose.orientation.x = pose->pose.orientation.x;
  goal_pose.pose.orientation.y = pose->pose.orientation.y;
  goal_pose.pose.orientation.z = pose->pose.orientation.z;
  goal_pose.pose.orientation.w = pose->pose.orientation.w;

  if (navigation_service_ != nullptr) {
    uint64_t request_id = navigation_service_->request_navigation(
        current_pose_, goal_pose, (uint8_t)Navigation_Type::P2P);
    if (request_id != 0) {
      RCLCPP_INFO(logger_, "request_navigation, request_id: %ld", request_id);
    } else {
      RCLCPP_INFO(logger_, "request_navigation failed");
    }
  }
}

rclcpp_action::GoalResponse NavigationActionServer::handle_qrb_goal(
    const rclcpp_action::GoalUUID &uuid,
    std::shared_ptr<const NavigateToPosition::Goal> goal) {
  point_2d target;
  target.x = goal->goal_x;
  target.y = goal->goal_y;
  target.angle = goal->goal_z;
  uint8_t type = (uint8_t)Navigation_Type::P2P;

  if (navigation_service_ != nullptr) {
    uint64_t request_id =
        navigation_service_->request_qrb_navigation(current_pose_, target, type);
    if (request_id != 0) {
      RCLCPP_INFO(logger_, "request_qrb_navigation, request_id: %ld", request_id);
      navigating_ = true;
      request_id_map_.insert(
          std::pair<rclcpp_action::GoalUUID, uint64_t>(uuid, request_id));
      return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
    } else {
      RCLCPP_INFO(logger_, "request_qrb_navigation failed");
    }
  }
  return rclcpp_action::GoalResponse::REJECT;
}

rclcpp_action::CancelResponse NavigationActionServer::handle_qrb_cancel(
    const std::shared_ptr<GoalHandleNavigateToPosition> goal_handle) {
  RCLCPP_INFO(logger_, "Received request to cancel qrb goal");

  if (navigation_service_ != nullptr) {
    if (navigation_service_->request_stop_p2p_navigation()) {
      return rclcpp_action::CancelResponse::ACCEPT;
    }
  }
  return rclcpp_action::CancelResponse::REJECT;
}

void NavigationActionServer::handle_qrb_accepted(
    const std::shared_ptr<GoalHandleNavigateToPosition> goal_handle) {
  rclcpp_action::GoalUUID goal_id = goal_handle->get_goal_id();
  uint64_t request_id = request_id_map_[goal_id];
  RCLCPP_INFO(logger_, "handle_qrb_accepted,request_id:%ld", request_id);
  qrb_handle_map_.insert(std::pair<uint64_t, std::shared_ptr<GoalHandleNavigateToPosition>>(
      request_id, goal_handle));
}

void NavigationActionServer::receive_qrb_navigation_callback(uint64_t request_id,
                                                         bool result) {
  RCLCPP_INFO(logger_, "receive_qrb_navigation_callback request_id:%ld, result:%d",
              request_id, result);
  if (!rclcpp::ok()) {
    return;
  }

  std::shared_ptr<GoalHandleNavigateToPosition> handle = qrb_handle_map_[request_id];

  auto action_result = std::make_shared<NavigateToPosition::Result>();
  if (handle == nullptr) {
    RCLCPP_INFO(logger_, "get qrb server global handle again");
    handle = qrb_server_global_handle_;
    if (handle == nullptr) {
      RCLCPP_INFO(logger_, "qrb server global handle is nullptr");
      return;
    }
  }

  if (result) {
    handle->succeed(action_result);
    RCLCPP_INFO(logger_, "Qrb goal succeeded");
  } else {
    if (handle->is_canceling()) {
      handle->canceled(action_result);
      RCLCPP_INFO(logger_, "Qrb goal canceled");
    } else {
      handle->abort(action_result);
      RCLCPP_INFO(logger_, "Qrb goal abort");
    }
  }

  request_id_map_.erase(handle->get_goal_id());
  qrb_handle_map_.erase(request_id);
  navigating_ = false;

  handle = nullptr;
  server_global_handle_ = nullptr;
}
} // namespace navigation
} // namespace qrb_ros