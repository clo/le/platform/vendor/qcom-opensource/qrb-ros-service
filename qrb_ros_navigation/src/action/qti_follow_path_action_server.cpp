/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include "action/qti_follow_path_action_server.hpp"

constexpr char const *node_name = "waypoint_follow_path_action_server";
constexpr char const *action_name = "wfollowpath";

namespace qrb_ros {
namespace navigation {
using namespace std::placeholders;

QTIFollowPathActionServer::QTIFollowPathActionServer(
    std::shared_ptr<NavigationService> &service)
    : Node(node_name), navigation_service_(service) {
  RCLCPP_INFO(logger_, "Creating");
  init_server();

  navigation_callback_ = [&](uint64_t request_id, bool result) {
    receive_navigation_callback(request_id, result);
  };
  navigation_service_->register_navigation_callback(navigation_callback_);
}

QTIFollowPathActionServer::~QTIFollowPathActionServer() {
  RCLCPP_INFO(logger_, "Destroying");
}

void QTIFollowPathActionServer::init_server() {
  action_server_ptr_ = rclcpp_action::create_server<QTIFollowPath>(
      this, action_name,
      std::bind(&QTIFollowPathActionServer::handle_goal, this, _1, _2),
      std::bind(&QTIFollowPathActionServer::handle_cancel, this, _1),
      std::bind(&QTIFollowPathActionServer::handle_accepted, this, _1));
}

rclcpp_action::GoalResponse QTIFollowPathActionServer::handle_goal(
    const rclcpp_action::GoalUUID &uuid,
    std::shared_ptr<const QTIFollowPath::Goal> goal) {
  RCLCPP_INFO(logger_, "Start follow path from amr controller");
  (void)uuid;
  uint32_t goal_id = goal->goal;

  passing_ids_.clear();
  current_passing_waypoint_id_ = 0;
  next_passing_waypoing_index_ = 0;
  current_distance_ = 0;
  last_distance_2_ = 0;
  last_distance_3_ = 0;
  uint32_t len = goal->passing_waypoint_ids.size();
  for (uint32_t i = 0; i < len; i++) {
    passing_ids_.push_back(goal->passing_waypoint_ids[i]);
  }

  if (navigation_service_ != nullptr) {
    uint64_t request_id =
        navigation_service_->request_follow_path(goal_id, passing_ids_);
    if (request_id != 0) {
      RCLCPP_INFO(logger_, "request follow path, request_id=%ld, uuid=%d", request_id, uuid);
      navigating_ = true;
      request_id_map_.insert(
          std::pair<rclcpp_action::GoalUUID, uint64_t>(uuid, request_id));
      return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
    } else {
      RCLCPP_INFO(logger_, "request qti follow path failed");
    }
  }
  return rclcpp_action::GoalResponse::REJECT;
}

rclcpp_action::CancelResponse QTIFollowPathActionServer::handle_cancel(
    const std::shared_ptr<GoalHandleQTIFollowPath> goal_handle) {
  RCLCPP_INFO(logger_, "Received request to cancel goal");
  (void)goal_handle;

  if (navigation_service_ != nullptr) {
    navigation_service_->request_stop_follow_path_navigation();
    return rclcpp_action::CancelResponse::ACCEPT;
  }
  return rclcpp_action::CancelResponse::REJECT;
}

void QTIFollowPathActionServer::handle_accepted(
    const std::shared_ptr<GoalHandleQTIFollowPath> goal_handle) {
  server_global_handle_ = goal_handle;

  rclcpp_action::GoalUUID uuid = goal_handle->get_goal_id();
  uint64_t request_id = request_id_map_[uuid];
  RCLCPP_INFO(logger_, "handle accepted, request_id=%ld, uuid=%d", request_id, uuid);
  handle_map_.insert(
      std::pair<uint64_t, std::shared_ptr<GoalHandleQTIFollowPath>>(
          request_id, goal_handle));
}

void QTIFollowPathActionServer::update_current_pose(PoseStamped &pose) {
  if (!navigating_) {
    RCLCPP_DEBUG(logger_, "current is not navigating");
    return;
  }

  if (!is_pose_changed(pose)) {
    RCLCPP_INFO(logger_, "Ingore pose update due to no update");
    return;
  }

  // feedback
  uint32_t len = handle_map_.size();
  if (len != 0) {
    auto feedback = std::make_shared<QTIFollowPath::Feedback>();
    feedback->current_pose = pose;
    feedback->passing_waypoint_id = get_passing_waypoint(pose);

    std::shared_ptr<GoalHandleQTIFollowPath> handle;

    RCLCPP_DEBUG(logger_, "handle_map_ size = %d", len);

    for (auto it : handle_map_) {
      handle = it.second;
      if (handle != nullptr && !(handle->is_canceling())) {
        handle->publish_feedback(feedback);
        RCLCPP_INFO(this->get_logger(), "feedback current pose(%.2f,%.2f), passing_waypint=%d",
                  pose.pose.position.x, pose.pose.position.y, feedback->passing_waypoint_id);
      }
    }
  }
}

bool QTIFollowPathActionServer::is_pose_changed(PoseStamped &pose) {
  bool is_changed = true;
  if (current_pose_.pose.position.x == pose.pose.position.x &&
      current_pose_.pose.position.y == pose.pose.position.y &&
      current_pose_.pose.position.z == pose.pose.position.z &&
      current_pose_.pose.orientation.x == pose.pose.orientation.x &&
      current_pose_.pose.orientation.y == pose.pose.orientation.y &&
      current_pose_.pose.orientation.z == pose.pose.orientation.z &&
      current_pose_.pose.orientation.w == pose.pose.orientation.w) {
    is_changed = false;
  }

  current_pose_.pose.position.x = pose.pose.position.x;
  current_pose_.pose.position.y = pose.pose.position.y;
  current_pose_.pose.position.z = pose.pose.position.z;
  current_pose_.pose.orientation.x = pose.pose.orientation.x;
  current_pose_.pose.orientation.y = pose.pose.orientation.y;
  current_pose_.pose.orientation.z = pose.pose.orientation.z;
  current_pose_.pose.orientation.w = pose.pose.orientation.w;
  current_pose_.header.stamp = pose.header.stamp;
  current_pose_.header.frame_id = pose.header.frame_id;

  return is_changed;
}

void QTIFollowPathActionServer::receive_navigation_callback(uint64_t request_id,
                                                            bool result) {
  RCLCPP_INFO(logger_, "receive_navigation_callback request_id:%ld, %d",
              request_id, result);
  if (!rclcpp::ok()) {
    return;
  }

  std::shared_ptr<GoalHandleQTIFollowPath> handle = handle_map_[request_id];

  auto action_result = std::make_shared<QTIFollowPath::Result>();
  if (handle == nullptr) {
    RCLCPP_INFO(logger_, "get server global handle again");
    handle = server_global_handle_;
    if (handle == nullptr) {
      RCLCPP_INFO(logger_, "server global handle is nullptr");
      return;
    }
  }

  if (result) {
    handle->succeed(action_result);
    RCLCPP_INFO(logger_, "Goal succeeded");
  } else {
    if (handle->is_canceling()) {
      handle->canceled(action_result);
      RCLCPP_INFO(logger_, "Goal canceled");
    } else {
      handle->abort(action_result);
      RCLCPP_INFO(logger_, "Goal abort");
    }
  }

  request_id_map_.erase(handle->get_goal_id());
  handle_map_.erase(request_id);
  navigating_ = false;

  handle = nullptr;
  server_global_handle_ = nullptr;
}

uint32_t QTIFollowPathActionServer::get_passing_waypoint(PoseStamped &pose) {
  point_2d current_point;
  convert_pose_to_2d_point(pose, current_point);

  uint32_t passing_id = get_passing_waypoint_id(current_point);
  return passing_id;
}

void QTIFollowPathActionServer::convert_pose_to_2d_point(PoseStamped &pose,
                                                         point_2d &point) {
  point.x = pose.pose.position.x;
  point.y = pose.pose.position.y;
  tf2::Quaternion quat(pose.pose.orientation.x, pose.pose.orientation.y,
                       pose.pose.orientation.z, pose.pose.orientation.w);
  double roll, pitch, yaw;
  tf2::Matrix3x3 m(quat);
  m.getRPY(roll, pitch, yaw);
  // dwa uses radians
  point.angle = yaw;

  RCLCPP_DEBUG(logger_, "Convert pose to position(%lf, %lf, %lf) on world map",
               point.x, point.y, point.angle);
}

uint32_t
QTIFollowPathActionServer::get_passing_waypoint_id(point_2d &current_point) {
  uint32_t size = passing_ids_.size();
  if (size == 0) {
    RCLCPP_DEBUG(logger_, "Find passing through waypoint failed due to no ids");
    return 0;
  }

  uint32_t id = navigation_service_->get_passing_waypoint_id(current_point);
  if (id != 0) {
    RCLCPP_INFO(logger_, "Find passing through waypoint(%d)", id);
  }
  return id;
}
} // namespace navigation
} // namespace qrb_ros