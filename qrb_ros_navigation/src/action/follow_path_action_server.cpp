/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include "action/follow_path_action_server.hpp"

constexpr char const *node_name = "follow_path_action_server";
constexpr char const *action_name = "followpath";

constexpr char const *service_name = "follow_path_sub_cmd";

namespace qrb_ros {
namespace navigation {
using namespace std::placeholders;

FollowPathActionServer::FollowPathActionServer(
    std::shared_ptr<NavigationService> &service)
    : Node(node_name), navigation_service_(service) {
  RCLCPP_INFO(this->get_logger(), "Creating");
  init_server();

  navigation_callback_ = [&](uint64_t request_id, bool result) {
    receive_navigation_callback(request_id, result);
  };
  navigation_service_->register_navigation_callback(navigation_callback_);
}

FollowPathActionServer::~FollowPathActionServer() {
  RCLCPP_INFO(this->get_logger(), "Destroying");
}

void FollowPathActionServer::init_server() {
  action_server_ptr_ = rclcpp_action::create_server<FollowPath>(
      this, action_name,
      std::bind(&FollowPathActionServer::handle_goal, this, _1, _2),
      std::bind(&FollowPathActionServer::handle_cancel, this, _1),
      std::bind(&FollowPathActionServer::handle_accepted, this, _1));

  service_ = this->create_service<FollowPathSubCmd>(
      service_name,
      std::bind(&FollowPathActionServer::receive_request, this, _1, _2));
}

rclcpp_action::GoalResponse FollowPathActionServer::handle_goal(
    const rclcpp_action::GoalUUID &uuid,
    std::shared_ptr<const FollowPath::Goal> goal) {
  RCLCPP_INFO(this->get_logger(), "Start follow path from amr controller");
  (void)uuid;
  nav_msgs::msg::Path path = goal->path;

  if (navigation_service_ != nullptr) {
    uint64_t request_id = navigation_service_->request_follow_path(path);
    if (request_id != 0) {
      RCLCPP_INFO(this->get_logger(), "request follow path, request_id: %ld",
                  request_id);
      navigating_ = true;
      request_id_map_.insert(
          std::pair<rclcpp_action::GoalUUID, uint64_t>(uuid, request_id));
      return rclcpp_action::GoalResponse::ACCEPT_AND_EXECUTE;
    }
  }
  return rclcpp_action::GoalResponse::REJECT;
}

rclcpp_action::CancelResponse FollowPathActionServer::handle_cancel(
    const std::shared_ptr<GoalHandleFollow> goal_handle) {
  RCLCPP_INFO(this->get_logger(), "Received request to cancel goal");
  (void)goal_handle;

  if (navigation_service_ != nullptr) {
    navigation_service_->request_stop_follow_path_navigation();
    return rclcpp_action::CancelResponse::ACCEPT;
  }
  return rclcpp_action::CancelResponse::REJECT;
}

void FollowPathActionServer::handle_accepted(
    const std::shared_ptr<GoalHandleFollow> goal_handle) {
  RCLCPP_INFO(this->get_logger(), "handle accepted");
  server_global_handle_ = goal_handle;

  rclcpp_action::GoalUUID goal_id = goal_handle->get_goal_id();
  uint64_t request_id = request_id_map_[goal_id];
  handle_map_.insert(std::pair<uint64_t, std::shared_ptr<GoalHandleFollow>>(
      request_id, goal_handle));
}

void FollowPathActionServer::update_current_pose(PoseStamped &pose) {
  if (!navigating_) {
    RCLCPP_DEBUG(logger_, "current is not navigating");
    return;
  }

  if (!is_pose_changed(pose)) {
    RCLCPP_INFO(logger_, "Ingore pose update due to no update");
    return;
  }

  // feedback
  // Follow path does not support current pose, nothing to do
}

bool FollowPathActionServer::is_pose_changed(PoseStamped &pose) {
  bool is_changed = true;
  if (current_pose_.pose.position.x == pose.pose.position.x &&
      current_pose_.pose.position.y == pose.pose.position.y &&
      current_pose_.pose.position.z == pose.pose.position.z &&
      current_pose_.pose.orientation.x == pose.pose.orientation.x &&
      current_pose_.pose.orientation.y == pose.pose.orientation.y &&
      current_pose_.pose.orientation.z == pose.pose.orientation.z &&
      current_pose_.pose.orientation.w == pose.pose.orientation.w) {
    is_changed = false;
  }

  current_pose_.pose.position.x = pose.pose.position.x;
  current_pose_.pose.position.y = pose.pose.position.y;
  current_pose_.pose.position.z = pose.pose.position.z;
  current_pose_.pose.orientation.x = pose.pose.orientation.x;
  current_pose_.pose.orientation.y = pose.pose.orientation.y;
  current_pose_.pose.orientation.z = pose.pose.orientation.z;
  current_pose_.pose.orientation.w = pose.pose.orientation.w;
  current_pose_.header.stamp = pose.header.stamp;
  current_pose_.header.frame_id = pose.header.frame_id;

  return is_changed;
}

void FollowPathActionServer::receive_navigation_callback(uint64_t request_id,
                                                         bool result) {
  RCLCPP_INFO(this->get_logger(),
              "receive_navigation_callback request_id:%ld, %d", request_id,
              result);
  if (!rclcpp::ok()) {
    return;
  }

  std::shared_ptr<GoalHandleFollow> handle = handle_map_[request_id];

  auto action_result = std::make_shared<FollowPath::Result>();
  if (handle == nullptr) {
    RCLCPP_INFO(logger_, "get server global handle again");
    handle = server_global_handle_;
    if (handle == nullptr) {
      RCLCPP_INFO(logger_, "server global handle is nullptr");
      return;
    }
  }

  if (result) {
    handle->succeed(action_result);
    RCLCPP_INFO(this->get_logger(), "Goal succeeded");
  } else {
    if (handle->is_canceling()) {
      handle->canceled(action_result);
      RCLCPP_INFO(this->get_logger(), "Goal canceled");
    } else {
      handle->abort(action_result);
      RCLCPP_INFO(this->get_logger(), "Goal abort");
    }
  }

  request_id_map_.erase(handle->get_goal_id());
  handle_map_.erase(request_id);
  navigating_ = false;

  handle = nullptr;
  server_global_handle_ = nullptr;
}

void FollowPathActionServer::receive_request(
    const std::shared_ptr<FollowPathSubCmd::Request> request,
    std::shared_ptr<FollowPathSubCmd::Response> response) {
  int cmd = request->subcommand;
  RCLCPP_INFO(logger_, "sub cmd: %s",
              StringUtil::subcmd_to_string(cmd).c_str());

  if (cmd == (int)SubCommand::PAUSE) {
    navigation_service_->request_pause_follow_path();
  } else if (cmd == (int)SubCommand::RESUME) {
    navigation_service_->request_resume_follow_path();
  } else {
    RCLCPP_INFO(logger_, "error: unkown sub command");
    response->result = false;
    return;
  }
  response->result = true;
}
} // namespace navigation
} // namespace qrb_ros