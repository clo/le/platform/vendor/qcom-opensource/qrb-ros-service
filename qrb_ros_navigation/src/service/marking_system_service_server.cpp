/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include "service/marking_system_service_server.hpp"
#include <float.h>

constexpr char const *node_name = "marking_system_service_server";
constexpr char const *service_name = "marking_system";

using namespace std::placeholders;
namespace qrb_ros {
namespace navigation {
MarkingSystemServiceServer::MarkingSystemServiceServer(
    std::shared_ptr<NavigationService> &service)
    : Node(node_name), navigation_service_(service) {
  RCLCPP_INFO(logger_, "Creating");
  init_server();
}

MarkingSystemServiceServer::~MarkingSystemServiceServer() {
  RCLCPP_INFO(logger_, "Destroying");
}

void MarkingSystemServiceServer::init_server() {
  service_ = this->create_service<NarrowArea>(
      service_name,
      std::bind(&MarkingSystemServiceServer::receive_request, this, _1, _2));
}

void MarkingSystemServiceServer::receive_request(
    const std::shared_ptr<NarrowArea::Request> request,
    std::shared_ptr<NarrowArea::Response> response) {
  uint8_t api_id = parse_request_to_api(request);
  RCLCPP_INFO(logger_, "api id: %s",
              StringUtil::api_id_to_string(api_id).c_str());

  if (navigation_service_ == nullptr) {
    RCLCPP_ERROR(logger_, "mannual_marker is nullptr");
    return;
  }
}

uint8_t MarkingSystemServiceServer::parse_request_to_api(
    const std::shared_ptr<NarrowArea::Request> request) {
  uint8_t api_id = request->api_id;
  return api_id;
}

uint8_t MarkingSystemServiceServer::parse_request_to_area_id(
    const std::shared_ptr<NarrowArea::Request> request) {
  uint8_t narrow_area_id = request->narrow_area_id;
  return narrow_area_id;
}

void MarkingSystemServiceServer::parse_request_to_narrow_area(
    const std::shared_ptr<NarrowArea::Request> &request, narrow_area &area) {
  area.point1_x = request->narrow_area[0];
  area.point1_y = request->narrow_area[1];
  area.point2_x = request->narrow_area[2];
  area.point2_y = request->narrow_area[3];
  area.point3_x = request->narrow_area[4];
  area.point3_y = request->narrow_area[5];
  area.point4_x = request->narrow_area[6];
  area.point4_y = request->narrow_area[7];
}

void MarkingSystemServiceServer::parse_request_to_2d_point(
    const std::shared_ptr<NarrowArea::Request> &request, point_2d &point) {
  point.x = request->point.x;
  point.y = request->point.y;
  point.angle = request->point.z;
}

void MarkingSystemServiceServer::convert_pose_to_2d_point(Pose pose,
                                                          point_2d &point) {
  double PI = 3.14;
  point.x = pose.pose.position.x;
  point.y = pose.pose.position.y;
  tf2::Quaternion quat(pose.pose.orientation.x, pose.pose.orientation.y,
                       pose.pose.orientation.z, pose.pose.orientation.w);
  double roll, pitch, yaw;
  tf2::Matrix3x3 m(quat);
  m.getRPY(roll, pitch, yaw);
  point.angle = yaw * 180 / PI;
}

bool MarkingSystemServiceServer::is_area_valid(narrow_area &area) {
  // if user only sets point, amr so library sets narrow area is DBL_MAX
  if (area.point1_x == DBL_MAX) {
    return false;
  }
  return true;
}

bool MarkingSystemServiceServer::is_point_valid(point_2d &point) {
  // if user only sets narrow area, amr so library sets point is DBL_MAX
  if ((point.x == DBL_MAX)) {
    return false;
  }
  return true;
}
} // namespace navigation
} // namespace qrb_ros