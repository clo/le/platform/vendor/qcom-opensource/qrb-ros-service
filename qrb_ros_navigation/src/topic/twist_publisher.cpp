/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include "twist_publisher.hpp"

namespace qrb_ros {
namespace navigation {
TwistPublisher::TwistPublisher(std::shared_ptr<NavigationService> &service)
    : Node("twist_pub"), navigation_service_(service) {
  init_publisher();
}

TwistPublisher::~TwistPublisher() {
  RCLCPP_DEBUG(logger_, "Destructor TwistPublisher object");
}

void TwistPublisher::init_publisher() {
  RCLCPP_DEBUG(logger_, "init twist publisher");
  pub_ = create_publisher<geometry_msgs::msg::Twist>("cmd_vel", 10);

  publish_twist_cb_ = [&](geometry_msgs::msg::Twist &velocity) {
    vel_mtx_.lock();
    send_velocity(velocity);
    vel_mtx_.unlock();
  };
  navigation_service_->register_publish_twist_callback(publish_twist_cb_);

  publish_uninterruptable_twist_cb_ = [&](geometry_msgs::msg::Twist& velocity, uint32_t duration) {
    vel_mtx_.lock();
    send_velocity(velocity);
    RCLCPP_INFO(logger_, "Start uninterruptable velocity on robot");

    usleep(duration * 1000);  // sleep duration ms

    geometry_msgs::msg::Twist twist;
    twist.linear.x = 0;
    twist.linear.y = 0;
    twist.linear.z = 0;
    twist.angular.x = 0;
    twist.angular.y = 0;
    twist.angular.z = 0;
    send_velocity(twist);
    RCLCPP_INFO(logger_, "Stop uninterruptable velocity on robot");
    vel_mtx_.unlock();
  };
  navigation_service_->register_publish_uninterruptable_twist_callback(publish_uninterruptable_twist_cb_);
}

void TwistPublisher::send_velocity(geometry_msgs::msg::Twist &velocity) {
  RCLCPP_INFO(logger_, "send velocity(%.2f, %.2f, %.2f) to robot",
              velocity.linear.x, velocity.linear.y, velocity.angular.z);
  pub_->publish(velocity);
}

} // namespace navigation
} // namespace qrb_ros