/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include "path_publisher.hpp"

namespace qrb_ros {
namespace navigation {
void PathPublisher::publish_global_path(const std::vector<point_2d> &point_list,
                                        double &origin_x, double &origin_y) {
  RCLCPP_DEBUG(logger_, "origin_x = %f, origin_y = %f", origin_x, origin_y);
  nav_msgs::msg::Path path;
  geometry_msgs::msg::PoseStamped pose;
  auto time = get_clock()->now();
  std_msgs::msg::Header header;
  header.frame_id = "/map";
  header.stamp.sec = time.seconds();
  header.stamp.nanosec = time.nanoseconds() % 1000000000;
  path.header = header;
  auto size = point_list.size();
  for (size_t i = 0; i < size; i++) {
    pose.pose.position.x = point_list.at(i).x;
    pose.pose.position.y = point_list.at(i).y;
    pose.header.stamp = header.stamp;
    path.poses.push_back(pose);
  }
  global_pub_->publish(path);
}

void PathPublisher::publish_real_path(const std::vector<point_2d> &point_list) {
  nav_msgs::msg::Path path;
  geometry_msgs::msg::PoseStamped pose;
  auto time = get_clock()->now();
  std_msgs::msg::Header header;
  header.frame_id = "/map";
  header.stamp.sec = time.seconds();
  header.stamp.nanosec = time.nanoseconds() % 1000000000;
  path.header = header;
  auto size = point_list.size();
  for (size_t i = 0; i < size; i++) {
    pose.pose.position.x = point_list.at(i).x;
    pose.pose.position.y = point_list.at(i).y;
    pose.header.stamp = header.stamp;
    path.poses.push_back(pose);
  }
  real_pub_->publish(path);
}

void PathPublisher::publish_local_path(const std::vector<point_2d> &point_list,
                                       double &origin_x, double &origin_y) {
  nav_msgs::msg::Path path;
  geometry_msgs::msg::PoseStamped pose;
  auto time = get_clock()->now();
  std_msgs::msg::Header header;
  header.frame_id = "/map";
  header.stamp.sec = time.seconds();
  header.stamp.nanosec = time.nanoseconds() % 1000000000;
  path.header = header;
  auto size = point_list.size();
  for (size_t i = 0; i < size; i++) {
    pose.pose.position.x = point_list.at(i).x + origin_x;
    pose.pose.position.y = point_list.at(i).y + origin_y;
    pose.header.stamp = header.stamp;
    path.poses.push_back(pose);
  }
  local_pub_->publish(path);
}

void PathPublisher::publish_local_shape_path(
    const std::vector<point_2d> &point_list, double &origin_x,
    double &origin_y) {
  nav_msgs::msg::Path path;
  geometry_msgs::msg::PoseStamped pose;
  auto time = get_clock()->now();
  std_msgs::msg::Header header;
  header.frame_id = "/map";
  header.stamp.sec = time.seconds();
  header.stamp.nanosec = time.nanoseconds() % 1000000000;
  path.header = header;
  auto size = point_list.size();
  for (size_t i = 0; i < size; i++) {
    pose.pose.position.x = point_list.at(i).x + origin_x;
    pose.pose.position.y = point_list.at(i).y + origin_y;
    pose.header.stamp = header.stamp;
    path.poses.push_back(pose);
  }
  local_shape_pub_->publish(path);
}

void PathPublisher::publish_backward_shape_path(
  const std::vector<point_2d>& point_list, double& origin_x, double& origin_y)
{
  nav_msgs::msg::Path path;
  geometry_msgs::msg::PoseStamped pose;
  auto time = get_clock()->now();
  std_msgs::msg::Header header;
  header.frame_id = "/map";
  header.stamp.sec = time.seconds();
  header.stamp.nanosec = time.nanoseconds() % 1000000000;
  path.header = header;
  auto size = point_list.size();
  for (size_t i = 0; i < size; i++) {
    pose.pose.position.x = point_list.at(i).x + origin_x;
    pose.pose.position.y = point_list.at(i).y + origin_y;
    pose.header.stamp = header.stamp;
    path.poses.push_back(pose);
  }
  backward_pub_->publish(path);
}

void PathPublisher::publish_narrow_info(std::vector<point_2d> &point_list,
                                        bool is_area) {
  nav_msgs::msg::Path path;
  geometry_msgs::msg::PoseStamped pose;
  auto time = get_clock()->now();
  std_msgs::msg::Header header;
  header.frame_id = "/map";
  header.stamp.sec = time.seconds();
  header.stamp.nanosec = time.nanoseconds() % 1000000000;
  path.header = header;
  auto size = point_list.size();
  for (size_t i = 0; i < size; i++) {
    pose.pose.position.x = point_list.at(i).x;
    pose.pose.position.y = point_list.at(i).y;
    pose.header.stamp = header.stamp;
    path.poses.push_back(pose);
  }
  if (is_area) {
    area_pub_->publish(path);
  } else {
    narrow_pub_->publish(path);
  }
}

PathPublisher::PathPublisher(std::shared_ptr<NavigationService> &service)
    : Node("path_node"), navigation_service_(service) {
  global_pub_ = create_publisher<nav_msgs::msg::Path>("global_path", 10);
  local_pub_ = create_publisher<nav_msgs::msg::Path>("local_path", 10);
  local_shape_pub_ =
      create_publisher<nav_msgs::msg::Path>("local_shape_path", 10);
  narrow_pub_ = create_publisher<nav_msgs::msg::Path>("narrow_path", 10);
  area_pub_ = create_publisher<nav_msgs::msg::Path>("narrow_area", 10);
  real_pub_ = create_publisher<nav_msgs::msg::Path>("real_path", 10);
  backward_pub_ = create_publisher<nav_msgs::msg::Path>("backward_path", 10);

  publish_real_path_cb_ = [&](std::vector<point_2d> &path) {
    publish_real_path(path);
  };
  navigation_service_->register_publish_real_path_callback(
      publish_real_path_cb_);

  publish_global_path_cb_ = [&](std::vector<point_2d> &path, double x,
                                double y) { publish_global_path(path, x, y); };
  navigation_service_->register_publish_global_path_callback(
      publish_global_path_cb_);

  publish_local_path_cb_ = [&](std::vector<point_2d> &path, double x,
                               double y) { publish_local_path(path, x, y); };
  navigation_service_->register_publish_local_path_callback(
      publish_local_path_cb_);

  publish_local_shape_path_cb_ = [&](std::vector<point_2d> &path, double x,
                                     double y) {
    publish_local_shape_path(path, x, y);
  };
  navigation_service_->register_publish_local_shape_path_callback(
      publish_local_shape_path_cb_);

  publish_backward_shape_path_cb_ = [&](std::vector<point_2d>& path, double x, double y) {
    publish_backward_shape_path(path, x, y);
  };
  navigation_service_->register_publish_backward_shape_path_callback(publish_backward_shape_path_cb_);
}

PathPublisher::~PathPublisher() {}
} // namespace navigation
} // namespace qrb_ros