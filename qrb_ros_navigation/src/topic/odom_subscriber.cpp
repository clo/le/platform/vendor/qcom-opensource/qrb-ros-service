/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

#include "odom_subscriber.hpp"

namespace qrb_ros {
namespace navigation {
OdomSubscriber::OdomSubscriber(std::shared_ptr<NavigationService> &service)
    : Node("odom_sub"), navigation_service_(service) {
  init_subscriber();
}

OdomSubscriber::~OdomSubscriber() {
  RCLCPP_DEBUG(logger_, "Destructor OdomSubscriber object");
}

void OdomSubscriber::init_subscriber() {
  RCLCPP_INFO(logger_, "init odom subsrcier");

  using namespace std::placeholders;
  sub_ = create_subscription<nav_msgs::msg::Odometry>(
      "odom", rclcpp::SystemDefaultsQoS(),
      std::bind(&OdomSubscriber::odom_callback, this, std::placeholders::_1));

  get_robot_velocity_cb_ = [&](nav_2d_msgs::msg::Twist2D &twist) {
    get_robot_velocity(twist);
  };
  navigation_service_->register_get_robot_velocity_callback(
      get_robot_velocity_cb_);
}

void OdomSubscriber::get_robot_velocity(nav_2d_msgs::msg::Twist2D &twist) {
  std::lock_guard<std::mutex> lock(mutex_);
  twist = odom_velocity_.velocity;
}

void OdomSubscriber::odom_callback(
    const nav_msgs::msg::Odometry::SharedPtr msg) {
  std::lock_guard<std::mutex> lock(mutex_);
  odom_velocity_.header = msg->header;
  odom_velocity_.velocity.x = msg->twist.twist.linear.x;
  odom_velocity_.velocity.y = msg->twist.twist.linear.y;
  odom_velocity_.velocity.theta = msg->twist.twist.angular.z;
  RCLCPP_DEBUG(
      logger_,
      "=======odom_callback velocity_x:%f,velocity_y:%f,velocity_theta:%f",
      odom_velocity_.velocity.x, odom_velocity_.velocity.y,
      odom_velocity_.velocity.theta);
}

} // namespace navigation
} // namespace qrb_ros