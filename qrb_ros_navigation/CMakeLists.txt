cmake_minimum_required(VERSION 3.8)
project(qrb_ros_navigation)

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  add_compile_options(-g -Wall -Wextra -Wpedantic)
endif()

# find dependencies
find_package(ament_cmake_auto REQUIRED)
find_package(geometry_msgs REQUIRED)
find_package(nav_msgs REQUIRED)
find_package(std_msgs REQUIRED)
find_package(nav2_msgs REQUIRED)
find_package(rclcpp REQUIRED)
find_package(rclcpp_action REQUIRED)
find_package(rcutils REQUIRED)
find_package(tf2 REQUIRED)
find_package(nav_2d_msgs REQUIRED)
find_package(Eigen3 REQUIRED)
find_package(tf2_ros REQUIRED)
find_package(tf2_geometry_msgs REQUIRED)
find_package(qrb_ros_amr_msgs REQUIRED)
find_package(qrb_ros_navigation_msgs REQUIRED)
find_package(qrb_ros_robot_base_msgs REQUIRED)
find_package(message_filters REQUIRED)
find_package(sensor_msgs REQUIRED)
find_package(OpenCV REQUIRED)
find_package(rclcpp_lifecycle REQUIRED)
find_package(nav2_costmap_2d REQUIRED)
find_package(visualization_msgs REQUIRED)
ament_auto_find_build_dependencies()

include_directories(
    ${PROJECT_SOURCE_DIR}/include
    ${PROJECT_SOURCE_DIR}/include/topic
    ${PROJECT_SOURCE_DIR}/include/service
    ${PROJECT_SOURCE_DIR}/include/action
    ${PROJECT_SOURCE_DIR}/include/navigation_service
    ${EIGEN3_INCLUDE_DIRS}
    ${CMAKE_SYSROOT}/usr/include/qrb_navigation_manager/
    ${CMAKE_SYSROOT}/usr/include/qrb_navigation_manager/include/
    )
#include_directories(${JSON_INC_PATH})
add_definitions(${EIGEN3_DEFINITIONS})

set(source
  src/main.cpp
  src/navigation_controller.cpp
  src/topic/exception_subscriber.cpp
  src/topic/tf_subscriber.cpp
  src/topic/odom_subscriber.cpp
  src/topic/path_publisher.cpp
  src/topic/twist_publisher.cpp
  src/service/marking_system_service_server.cpp
  src/service/virtual_path_service_server.cpp
  src/service/navigation_path_service_server.cpp
  src/action/navigation_action_server.cpp
  src/action/follow_path_action_server.cpp
  src/action/qti_follow_path_action_server.cpp
)

aux_source_directory(./src SRC_LIST)

ament_auto_add_executable(qrb_ros_navigation ${SRC_LIST} ${source})

ament_target_dependencies(qrb_ros_navigation
  "nav_msgs"
  "nav2_msgs"
  "geometry_msgs"
  "rclcpp"
  "rclcpp_action"
  "std_msgs"
  "rcutils"
  "tf2"
  "nav_2d_msgs"
  "tf2_ros"
  "tf2_geometry_msgs"
  "qrb_ros_amr_msgs"
  "qrb_ros_navigation_msgs"
  "qrb_ros_robot_base_msgs"
  "OpenCV"
  "rclcpp_lifecycle"
  "nav2_costmap_2d"
  "visualization_msgs"
)

target_link_libraries(qrb_ros_navigation
  tinyxml2
)

target_link_libraries(qrb_ros_navigation
  qrb_navigation_manager
)

install(TARGETS qrb_ros_navigation
  RUNTIME DESTINATION lib/${PROJECT_NAME}
)

install(DIRECTORY
  config
  DESTINATION share/${PROJECT_NAME}/
)

install(DIRECTORY
  launch
  DESTINATION share/${PROJECT_NAME}/
)

ament_package()
